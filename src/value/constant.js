export const Document_Status = {
  waiting: 1,
  rejected: 2,
  verified: 3,
};

export const Document_Status_List = [
  {
    value: Document_Status.waiting,
    label: 'Waiting',
  },
  {
    value: Document_Status.rejected,
    label: 'Rejected',
  },
  {
    value: Document_Status.verified,
    label: 'Verified',
  },
];
