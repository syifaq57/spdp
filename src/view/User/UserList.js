/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {Container, Content, Card} from 'native-base';

import colors from '../../res/colors';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';

import firestore from '@react-native-firebase/firestore';

import {useNavigation} from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ReCardView from '../../component/ReCardView';

const initialState = {
  loading: true,
  users: [],
};

const UserList = () => {
  const [state, setState] = useState(initialState);

  const navigation = useNavigation();

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const getUsers = () => {
    return state.users;
  };

  const setUsers = (val) => {
    updateState({
      users: val,
    });
  };

  const onOpenUser = (user) => {
    navigation.navigate('TambahUser', {user: user});
  };

  const deleteUser = (user) => {
    const userCollection = firestore().collection('user');
    userCollection.doc(user.id).delete();
  };
  let unsub;

  const initialScreen = async () => {
    const userCollection = firestore().collection('user');
    if (unsub) {
      unsub();
    }
    unsub = await userCollection
      .orderBy('createdAt', 'desc')
      .onSnapshot((snapshot) => {
        let list = [];
        snapshot.forEach((doc) => {
          list.push({
            id: doc.id,
            image: doc.data().image,
            nama: doc.data().nama,
            username: doc.data().username,
            password: doc.data().password,
            role: doc.data().role,
            createdAt: doc.data().createdAt,
          });
        });
        setUsers(list);
      });
  };

  useEffect(() => {
    initialScreen();
  }, []);

  return (
    <View>
      {getUsers().map((data, index) => (
        <TouchableOpacity key={index} onPress={() => onOpenUser(data)}>
          <ReCardView
            style={{
              marginBottom: hp(2),
              borderRadius: wp(1.5),
              padding: wp(0.5),
              // backgroundColor: colors.primary,
            }}>
            <LinearGradient
              start={{x: 0.0, y: 0.0}}
              end={{x: 0.9, y: 0.9}}
              colors={[colors.secondary, colors.white]}
              style={{
                flexDirection: 'row',
                borderRadius: wp(1),
                height: hp(10),
                alignItems: 'center',
                paddingHorizontal: wp(3),
              }}>
              <View style={{flex: 1, marginRight: wp(1)}}>
                <Image
                  source={
                    data.image
                      ? {
                          uri: data.image,
                        }
                      : require('../../res/images/Profile.png')
                  }
                  style={{
                    height: hp(6),
                    width: hp(6),
                    borderRadius: hp(7),
                    resizeMode: 'cover',
                  }}
                />
              </View>
              <View style={{flex: 6, marginLeft: wp(2)}}>
                <Text
                  ellipsizeMode="tail"
                  numberOfLines={1}
                  style={{
                    color: colors.textGray,
                    fontSize: wp(3.3),
                    fontFamily: 'JosefinSans-Bold',
                    marginBottom: hp(0.5),
                  }}>
                  {data.nama}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <FontAweSome
                    style={{
                      textAlignVertical: 'center',
                      marginTop: hp(-0.2),
                      marginRight: wp(0.5),
                    }}
                    name={'gear'}
                    color={colors.secondary}
                    size={hp(1.8)}
                  />
                  <Text
                    style={{
                      fontSize: wp(3),
                      color: colors.textGray,
                      fontFamily: 'Poppins-Regular',
                    }}>
                    {data.role}
                  </Text>
                </View>
              </View>
              <View style={{flex: 0.5}}>
                <TouchableOpacity onPress={() => deleteUser(data)}>
                  <FontAweSome
                    name="trash"
                    color={colors.secondary}
                    size={hp(3.2)}
                  />
                </TouchableOpacity>
              </View>
            </LinearGradient>
          </ReCardView>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default UserList;
