/* eslint-disable react-native/no-inline-styles */
import React, {useState, useRef, useEffect} from 'react';

import {View} from 'react-native';

import DeafaultHeader from '../../component/DeafaultHeader';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import DefaultContainer from '../../component/DefaultContainer';
import ProfileForm from '../Profile/ProfileForm';

import firestore from '@react-native-firebase/firestore';
import {useRoute} from '@react-navigation/native';

const initialState = {
  nama: '',
  editMode: false,
};

const TambahUser = () => {
  const [state, setState] = useState(initialState);

  const profileForm = useRef(null);
  const route = useRoute();

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const loadUser = () => {
    return firestore().collection('user').doc('ya7QUh5kGpOh2w7pirjQ').get();
  };

  const initialScreen = async () => {
    const userParam = route?.params?.user;
    if (userParam) {
      updateState({
        editMode: true,
      });
    }
    profileForm.current?.setUser(userParam);
  };

  useEffect(() => {
    initialScreen();
  }, []);

  return (
    <DefaultContainer>
      <DeafaultHeader
        title={state.editMode ? 'Edit User' : 'Tambah User'}
        backButton
      />
      <View style={{flex: 1, paddingHorizontal: wp(8), paddingTop: hp(2)}}>
        <ProfileForm roleEditable ref={profileForm} />
      </View>
    </DefaultContainer>
  );
};

export default TambahUser;
