/* eslint-disable react-native/no-inline-styles */
import React, {useState, useRef, useEffect} from 'react';

import {View} from 'react-native';

import DeafaultHeader from '../../component/DeafaultHeader';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import DefaultContainer from '../../component/DefaultContainer';
import ProfileForm from '../Profile/ProfileForm';

import firestore from '@react-native-firebase/firestore';
import UserList from './UserList';
import DefaultContentView from '../../component/DefaultContentView';
import {useNavigation} from '@react-navigation/native';

const initialState = {
  nama: '',
};
const UserScreen = () => {
  const navigation = useNavigation();

  return (
    <DefaultContainer>
      <DeafaultHeader
        title="Data User"
        backButton
        rightButton="plus"
        onRightButtonPress={() => navigation.navigate('TambahUser')}
      />
      <DefaultContentView>
        <UserList />
      </DefaultContentView>
    </DefaultContainer>
  );
};

export default UserScreen;
