/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  Alert,
  BackHandler,
} from 'react-native';
import {Container, Text} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';

import AsyncStorage from '@react-native-async-storage/async-storage';

import ReTextInput from '../../component/ReTextInput';
import ReButton from '../../component/ReButton';
import ReLoadingModal from '../../component/ReLoadingModal';
import ReAlertModal from '../../component/ReAlertModal';
import colors from '../../res/colors/index';

import {useNavigation, useFocusEffect} from '@react-navigation/native';
import firestore from '@react-native-firebase/firestore';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const initialLogin = {
  username: '',
  password: '',
};

const LoginScreen = () => {
  const [loginForm, setLoginForm] = useState(initialLogin);
  const [state, setState] = useState({
    loading: false,
    successModal: false,

    titleDisplay: true,
  });

  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const LoginUser = async () => {
    await setLoading(true);
    let User;

    // const users = await firestore().collection('user').get();

    const userCollection = firestore().collection('user');

    await userCollection
      .where('username', '==', loginForm.username)
      .where('password', '==', loginForm.password)
      .get()
      .then(async (snapshot) => {
        await snapshot.forEach((doc) => {
          if (doc) {
            User = {
              id: doc.id,
              image: doc.data().image,
              nama: doc.data().nama,
              username: doc.data().username,
              password: doc.data().password,
              role: doc.data().role,
              createdAt: doc.data().createdAt,
            };
          }
        });
        if (User) {
          await AsyncStorage.setItem('userData', JSON.stringify(User));
          await navigation.navigate('HomeScreen');
        } else {
          Alert.alert('Gagal Login', 'Username / Password tidak sesuai');
        }
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        Alert.alert('Gagal Login', 'Check Koneksi Internet');
        setLoading(false);
      });
  };

  const checkFormLogin = () => {
    if (loginForm.username.length > 0 && loginForm.password.length > 0) {
      return false;
    }
    return false;
  };

  const onKeyboardDidShow = () => {
    updateState({titleDisplay: false});
  };

  const onKeyboardDidHide = () => {
    updateState({titleDisplay: true});
  };

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', onKeyboardDidShow);
    Keyboard.addListener('keyboardDidHide', onKeyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', onKeyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', onKeyboardDidHide);
    };
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        Alert.alert('Exit App!', 'Apakah Kamu yakin ingin keluar Aplikasi?', [
          {
            text: 'Cancel',
            onPress: () => {
              // return true;
            },
            style: 'cancel',
          },
          {
            text: 'YES',
            onPress: () => {
              BackHandler.exitApp();
              // return false;
            },
          },
        ]);
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  return (
    <Container
      style={{
        flex: 1,
        backgroundColor: colors.white,
      }}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <LinearGradient
          colors={[
            colors.gradientPrimary,
            colors.gradientPrimary,
            colors.gradientSecondary,
          ]}
          style={{
            flex: 1,
            justifyContent: 'center',
            paddingHorizontal: wp(8),
          }}>
          <View
            style={{
              alignItems: 'center',
              marginTop: hp(-5),
            }}>
            <View style={{alignItems: 'center'}}>
              <View style={{marginBottom: wp(10), alignItems: 'center'}}>
                <Text
                  style={{
                    fontSize: wp(10),
                    // fontWeight: 'bold',
                    // marginBottom: hp(-1),
                    color: colors.gray06,
                    fontFamily: 'JosefinSans-Bold',
                  }}>
                  SPDP
                </Text>
                <Text
                  style={{
                    fontSize: wp(4.3),
                    color: colors.greenpln,
                    fontFamily: 'JosefinSans-Medium',
                  }}>
                  - Sistem Pemutakhiran Data Pemilihan -
                </Text>
              </View>
            </View>
            <View style={{width: wp(84)}}>
              <ReTextInput
                label="Username"
                value={loginForm.username}
                onChangeText={(val) => {
                  setLoginForm({...loginForm, username: val});
                }}
                loginScreen
                valueColor={colors.textSecondary}
                style={{marginBottom: hp(1.5)}}
              />
              <ReTextInput
                label="Password"
                value={loginForm.password}
                onChangeText={(val) => {
                  setLoginForm({...loginForm, password: val});
                }}
                passwordInput
                loginScreen
                valueColor={colors.textSecondary}
                style={{marginBottom: hp(4)}}
              />
            </View>

            <View style={{marginVertical: wp(4), width: wp(60)}}>
              <ReButton
                label="LOGIN"
                disabled={checkFormLogin()}
                onPress={() => {
                  LoginUser();
                }}
              />
            </View>
            <View
              style={{
                marginTop: wp(2),
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Text style={{color: colors.white}}>{'-SPDP- V.1.1'}</Text>
            </View>
          </View>
        </LinearGradient>
      </KeyboardAvoidingView>

      <ReLoadingModal visible={loading} title="Login.." />
      <ReAlertModal
        visible={state.successModal}
        title="Gagal Login"
        subTitle="Cek Email dan Password"
        onConfirm={() => {
          setState({...state, successModal: false});
        }}
      />
    </Container>
  );
};

export default LoginScreen;
