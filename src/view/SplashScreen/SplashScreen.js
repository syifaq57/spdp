/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {StyleSheet, Alert, ActivityIndicator} from 'react-native';
import {Container, View, Text} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import AsyncStorage from '@react-native-async-storage/async-storage';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    AsyncStorage.getItem('userData').then((item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        setTimeout(() => {
          navigation.navigate('HomeScreen');
        }, 3000);
      } else {
        setTimeout(() => {
          navigation.navigate('LoginScreen');
        }, 3000);
      }
    });
  }, []);

  return (
    <Container>
      <View
        style={{
          backgroundColor: 'black',
          flex: 1,
          justifyContent: 'center',
          padding: 20,
        }}>
        <View style={stylePage.viewPT}>
          <View style={stylePage.logoView}>
            <Text style={{color: 'white', fontSize: wp(5), fontWeight: 'bold'}}>
              SPDP
            </Text>
          </View>
          <View style={stylePage.logoView}>
            <View style={{paddingVertical: hp('2%')}}>
              <ActivityIndicator size={'large'} color="white" />
            </View>
          </View>
          {/* <View style={stylePage.logoView}>
            <Text style={{fontSize: wp('4.5%'), fontWeight: 'bold'}}>
              Loading . . . .
            </Text>
          </View> */}
        </View>
      </View>
    </Container>
  );
};

const stylePage = StyleSheet.create({
  viewPT: {
    marginHorizontal: wp('5%'),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -15,
  },
  logoView: {
    alignItems: 'center',
  },
  logo: {
    width: hp('20%'),
    height: hp('20%'),
    resizeMode: 'contain',
    // borderRadius: wp(30),
  },
  loading: {
    width: hp('10%'),
    height: hp('10%'),
    resizeMode: 'contain',
  },
});

export default SplashScreen;
