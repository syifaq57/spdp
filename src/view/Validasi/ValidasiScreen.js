/* eslint-disable react-native/no-inline-styles */
import React, {useRef} from 'react';

import DeafaultHeader from '../../component/DeafaultHeader';

import DefaultContainer from '../../component/DefaultContainer';

import DefaultContentView from '../../component/DefaultContentView';
import ListDocumentData from '../ExporImport/LisDocumentData';
import ModalDocFilter from '../ExporImport/ModalDocFilter';

const ValidasiScreen = () => {
  const listDocumentData = useRef(null);
  const modalDocFilter = useRef(null);

  const onSaveFilter = (proper) => {
    listDocumentData.current?.onChangeFilter(proper);
  };

  return (
    <DefaultContainer>
      <DeafaultHeader
        title={'Validasi Data'}
        backButton
        rightButton={'filter'}
        onRightButtonPress={() => {
          modalDocFilter.current?.show();
        }}
      />
      <DefaultContentView>
        {/* <TopMenuButtons onSaveFilter={onSaveFilter} /> */}
        <ListDocumentData ref={listDocumentData} validationType={true} />

        <ModalDocFilter ref={modalDocFilter} onSaveFilter={onSaveFilter} />
      </DefaultContentView>
    </DefaultContainer>
  );
};

export default ValidasiScreen;
