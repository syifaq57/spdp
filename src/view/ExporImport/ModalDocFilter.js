/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/self-closing-comp */
import {View, Text, StyleSheet, ScrollView, Alert} from 'react-native';
import React, {useState, useImperativeHandle, forwardRef} from 'react';

import Modal from 'react-native-modal';
import colors from '../../res/colors';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import RePicker from '../../component/RePicker';
import {Document_Status_List} from '../../value/constant';
import {TouchableOpacity} from 'react-native-gesture-handler';
import ReButton from '../../component/ReButton';
import ReDatePicker from '../../component/ReDatePicker';

const initialState = {
  isVisible: false,
  uploader: null,
  status: null,
  start_date: null,
  end_date: null,
};

const ModalDocFilter = ({onSaveFilter}, ref) => {
  const [state, setState] = useState(initialState);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const show = () => {
    updateState({
      isVisible: true,
    });
  };

  const onBackButtonPress = () => {
    updateState({
      isVisible: false,
    });
  };
  const _onSaveFilter = () => {
    if (state.start_date && !state.end_date) {
      Alert.alert('Gagal Save Filter', 'End Date tidak boleh kosong');
      return;
    }

    if (state.end_date && !state.start_date) {
      Alert.alert('Gagal Save Filter', 'Start Date tidak boleh kosong');
      return;
    }

    onSaveFilter({
      status: state.status,
      start: state.start_date,
      end: state.end_date,
    });

    setState(initialState);
  };

  useImperativeHandle(ref, () => ({
    show,
  }));

  return (
    <View>
      <Modal
        isVisible={state.isVisible}
        style={stylePage.container}
        backdropOpacity={0.3}
        animationIn={'slideInRight'}
        animationOut={'slideOutRight'}
        onBackdropPress={onBackButtonPress}
        onBackButtonPress={onBackButtonPress}>
        {/* header */}
        <View
          style={{
            height: hp(7),
            justifyContent: 'center',
            paddingHorizontal: wp(4),
            backgroundColor: colors.primary,
          }}>
          <Text
            style={{
              fontSize: wp(4.5),
              fontWeight: 'bold',
              color: colors.textSecondary,
            }}>
            FILTER
          </Text>
        </View>
        {/* filter list */}
        <ScrollView style={{paddingHorizontal: wp(4), flex: 1}}>
          {/* 
           time upload
            uploaded by
            status
          */}
          {/* <RePicker
            label="Uploader"
            items={[]}
            selectedValue={state.uploader}
            onValueChange={(value) => {
              updateState({
                uploader: value,
              });
            }}
          /> */}
          <ReDatePicker
            style={{marginTop: hp(2), marginBottom: hp(1.5)}}
            placeholder="Select Start Date"
            label="Start Date"
            selectedDate={state.start_date}
            onDateChange={(date) => {
              updateState({start_date: date});
            }}
          />
          <ReDatePicker
            style={{marginBottom: hp(2.5)}}
            placeholder="Select End Date"
            label="End Date"
            selectedDate={state.end_date}
            onDateChange={(date) => {
              updateState({end_date: date});
            }}
          />
          <RePicker
            label="Pilih Status"
            items={Document_Status_List}
            style={{borderColor: colors.primary}}
            selectedValue={state.status}
            noAnimation
            onValueChange={(value) => {
              updateState({
                status: value,
              });
            }}
          />
        </ScrollView>
        <View>
          <ReButton
            style={{
              marginHorizontal: wp(8),
              marginVertical: hp(4.5),
              paddingVertical: hp(1.2),
            }}
            label="Save Filter"
            disabled={false}
            onPress={_onSaveFilter}
          />
        </View>
      </Modal>
    </View>
  );
};

const stylePage = StyleSheet.create({
  container: {
    marginVertical: 0,
    backgroundColor: 'white',
    height: '100%',
    width: '65%',
    marginHorizontal: 0,
    alignSelf: 'flex-end',
    justifyContent: 'flex-start',
  },
});

export default forwardRef(ModalDocFilter);
