/* eslint-disable react-native/no-inline-styles */
import {View, Text, StyleSheet} from 'react-native';
import React, {
  useEffect,
  useState,
  useRef,
  useImperativeHandle,
  forwardRef,
} from 'react';

import AsyncStorage from '@react-native-async-storage/async-storage';

import DocumentPicker from 'react-native-document-picker';

import FontAweSome from 'react-native-vector-icons/FontAwesome';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {BaseButton} from 'react-native-gesture-handler';
import colors from '../../res/colors';

import XLSX from 'xlsx';

import RNFS, {writeFile, readFile, stat} from 'react-native-fs';

import excelToJson from 'convert-excel-to-json';
import ModalImportConfirmation from './ModalImportConfirmation';
import ModalDocFilter from './ModalDocFilter';

const TopMenuButtons = ({onSaveFilter}, ref) => {
  const [userData, setUserData] = useState(null);

  const modalImportConfirmation = useRef(null);
  const modalDocFilter = useRef(null);

  const onPressImport = async (doc) => {
    // picker document
    const res = await DocumentPicker.pickSingle({
      allowMultiSelection: false,
      // copyTo: 'documentDirectory',
      type: [DocumentPicker.types.xls, DocumentPicker.types.xlsx],
    });

    // convert xlsx to JSON
    readFile(res.uri, 'ascii').then((resfs) => {
      const wb = XLSX.read(resfs, {
        type: 'binary',
      });

      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      const data = XLSX.utils.sheet_to_csv(ws, {header: 1});

      const docToJson = convertToJson(data);

      modalImportConfirmation.current?.show(res.name, docToJson, doc);
    });
  };

  const convertToJson = (csv) => {
    var lines = csv.split('\n');

    var result = [];

    var headers = lines[0].split(',');
    // headers[0] = 'ID';

    for (var i = 1; i < lines.length; i++) {
      var obj = {};
      var currentline = lines[i].split(',');

      for (var j = 0; j < headers.length; j++) {
        obj[headers[j]] = currentline[j];
      }

      if (obj['"ID"']) {
        result.push(obj);
      }
    }

    return result.map((x) => {
      x.ID = x['"ID"'];

      delete x['"ID"'];

      return {
        ...x,
      };
    }); //JSON
  };

  const onOpenFilter = () => {
    modalDocFilter.current?.show();
  };

  const initialScreen = async () => {
    await AsyncStorage.getItem('userData').then(async (item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        setUserData(user);
      }
    });
  };

  useEffect(() => {
    initialScreen();
  }, []);

  useImperativeHandle(ref, () => ({
    onPressImport,
  }));

  if (!['admin', 'kpu', 'ppdp'].includes(userData?.role)) {
    return null;
  }

  return (
    <View
      style={{
        marginTop: hp(-0.5),
        marginBottom: hp(1),
        flexDirection: 'row',
      }}>
      <BaseButton
        onPress={onPressImport}
        style={{...stylePage.ButtonMenu, marginRight: wp(1)}}>
        <Text style={stylePage.TextButton}>IMPORT DOCUMENT</Text>
        <FontAweSome
          name={'plus'}
          color={colors.textSecondary}
          size={hp(2.5)}
        />
      </BaseButton>
      {/* <BaseButton
        onPress={onOpenFilter}
        style={{
          ...stylePage.ButtonMenu,
          flex: 0.2,
          flexDirection: 'column',
          alignItems: 'center',
          marginLeft: wp(1),
        }}>
        <Text style={stylePage.TextButton}>FILTER</Text>
      </BaseButton> */}
      <ModalImportConfirmation
        ref={modalImportConfirmation}
        userData={userData}
      />
      <ModalDocFilter ref={modalDocFilter} onSaveFilter={onSaveFilter} />
    </View>
  );
};

export default forwardRef(TopMenuButtons);

const stylePage = StyleSheet.create({
  ButtonMenu: {
    backgroundColor: colors.primary,
    borderRadius: wp(1),
    paddingVertical: hp(1.3),
    flex: 1,
    paddingHorizontal: wp(3),
    flexDirection: 'row',
    // alignItems: 'center',
  },
  TextButton: {
    flex: 1,
    fontSize: wp(3.5),
    fontWeight: 'bold',
    color: colors.textSecondary,
  },
});
