/* eslint-disable react-native/no-inline-styles */
import {View, Text, TouchableOpacity, ActivityIndicator} from 'react-native';
import React, {useState, forwardRef, useImperativeHandle} from 'react';
import ReAlertModal from '../../component/ReAlertModal';
import Modal from 'react-native-modal';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import colors from '../../res/colors';

import firestore from '@react-native-firebase/firestore';

import uuid from 'react-native-uuid';

const initialState = {
  name: '',
  data: [],
  ref_doc: null,
};

const ModalImportConfirmation = ({userData}, ref) => {
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  const [state, setState] = useState(initialState);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const createDocumentData = async () => {
    if (state.ref_doc) {
      await firestore()
        .collection('document')
        .doc(state.ref_doc.id)
        .update({
          total_doc: state.data.length,
          pemilih: state.data.map((item) => {
            item.id_doc_ref = state.ref_doc.id;
            item._id = uuid.v1();
            return item;
          }),
          status_key: 1,
        })
        .then(() => {})
        .catch(() => {
          alert('Gagal menyimpan data');
        });

      return docData;
    }

    let id_ref = uuid.v1();

    const docData = {
      createdAt: firestore.Timestamp.fromDate(new Date()),
      createdBy: userData.id || '',
      createdName: userData.nama,
      ref_id: id_ref,
      total_doc: state.data.length,
      pemilih: state.data.map((item) => {
        item.id_doc_ref = id_ref;
        item._id = uuid.v1();
        return item;
      }),
      status_key: 1,
      url: '',
    };

    await firestore()
      .collection('document')
      .doc(id_ref)
      .set(docData)
      .then(() => {})
      .catch(() => {
        alert('Gagal menyimpan document');
      });

    return docData;
  };

  const _onConfirm = async () => {
    setLoading(true);
    try {
      if (state.data.length > 0) {
        await createDocumentData();
      }
      setVisible(false);
      setLoading(false);
      setState(initialState);
    } catch (e) {
      console.log('error', e);
      setLoading(false);
    }
  };

  const show = (docName, docData, ref_doc) => {
    setVisible(true);
    updateState({
      name: docName,
      data: docData,
      ref_doc: ref_doc,
    });
  };

  useImperativeHandle(ref, () => ({
    show,
  }));

  return (
    <Modal
      style={{alignItems: 'center'}}
      backdropOpacity={0.4}
      animationIn={'fadeIn'}
      animationOut={'fadeOut'}
      onBackdropPress={() => setVisible(false)}
      isVisible={visible}>
      <View
        style={{
          backgroundColor: 'white',
          borderRadius: wp('2%'),
          padding: hp(2),
          paddingBottom: wp(4),
          width: wp(85),
          top: hp(-5),
        }}>
        <View
          style={{
            paddingBottom: hp('1%'),
            borderBottomWidth: 0.5,
            borderColor: 'gray',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: wp(4), fontWeight: 'bold'}}>Import Data</Text>
        </View>
        <View
          style={{
            paddingTop: hp(3.5),
            paddingBottom: hp(4.5),
            marginTop: hp(-0.5),
            alignItems: 'center',
          }}>
          <Text style={{fontSize: wp(3.5)}}>
            {`Apa anda yakin ingin import dokumen ${state.name} ke database ?`}
          </Text>
        </View>
        <View
          style={{
            alignItems: 'center',
            marginBottom: hp(1),
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            disabled={loading}
            style={{
              flex: 1,
              marginRight: wp(5),

              borderRadius: wp(1),
              alignItems: 'center',
              justifyContent: 'center',
              // width: wp(16),
              height: wp(8),
              // borderWidth: 1,
              backgroundColor: colors.textTertiary,
            }}
            onPress={() => setVisible(false)}>
            <Text
              style={{
                fontSize: wp('3%'),
                color: colors.white,
                fontWeight: 'bold',
              }}>
              Cancel
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flex: 1,
              marginLeft: wp(5),
              borderRadius: wp(1),
              alignItems: 'center',
              justifyContent: 'center',
              width: wp(16),
              height: wp(8),
              // borderWidth: 1,
              backgroundColor: colors.primary,
            }}
            disabled={loading}
            onPress={_onConfirm}>
            {loading ? (
              <ActivityIndicator size={'small'} color="wite" />
            ) : (
              <Text
                style={{
                  fontSize: wp('3%'),
                  color: colors.white,
                  fontWeight: 'bold',
                }}>
                Import
              </Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default forwardRef(ModalImportConfirmation);
