/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState} from 'react';

import DeafaultHeader from '../../component/DeafaultHeader';

import DefaultContainer from '../../component/DefaultContainer';

import DefaultContentView from '../../component/DefaultContentView';
import TopMenuButtons from './TopMenuButtons';
import ListDocumentData from './LisDocumentData';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ModalDocFilter from './ModalDocFilter';

const ExportImportScreen = () => {
  const listDocumentData = useRef(null);

  const topMenuButtons = useRef(null);

  const modalDocFilter = useRef(null);

  const onImportDoc = (doc) => {
    topMenuButtons.current?.onPressImport(doc);
  };

  const onSaveFilter = (proper) => {
    listDocumentData.current?.onChangeFilter(proper);
  };

  return (
    <DefaultContainer>
      <DeafaultHeader
        title={'Export Data'}
        backButton
        rightButton={'filter'}
        onRightButtonPress={() => {
          modalDocFilter.current?.show();
        }}
      />

      <DefaultContentView>
        <TopMenuButtons ref={topMenuButtons} />
        <ListDocumentData ref={listDocumentData} onImportDoc={onImportDoc} />
      </DefaultContentView>
      <ModalDocFilter ref={modalDocFilter} onSaveFilter={onSaveFilter} />
    </DefaultContainer>
  );
};

export default ExportImportScreen;
