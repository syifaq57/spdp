/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {
  useState,
  useImperativeHandle,
  forwardRef,
  useEffect,
} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {Container, Content, Card} from 'native-base';

import colors from '../../res/colors';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import LinearGradient from 'react-native-linear-gradient';

import firestore from '@react-native-firebase/firestore';

import {useNavigation} from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ReCardView from '../../component/ReCardView';
import exportExcel from '../../handler/ExportDoc';
import ReLoadingModal from '../../component/ReLoadingModal';

import {Document_Status} from '../../value/constant';
import ReLoading from '../../component/ReLoading';
import AsyncStorage from '@react-native-async-storage/async-storage';

const initialState = {
  loading: false,
  loadingexprt: false,
  docs: [],
  status: null,
  start: null,
  end: null,
};

const ListDocumentData = ({validationType, onImportDoc}, ref) => {
  const [state, setState] = useState(initialState);

  const navigation = useNavigation();

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const isLoading = () => {
    return state.loading;
  };

  const setLoading = (flag) => {
    updateState({
      loading: flag,
    });
  };

  const setDocs = (val) => {
    updateState({
      docs: val,
    });
  };

  const getDocs = () => {
    return state.docs;
  };

  const setLoadingExport = (flag) => {
    updateState({
      loadingexprt: flag,
    });
  };

  const onOpenDoc = (data) => {
    if (data.status_key == Document_Status.rejected) {
      Alert.alert(
        'Document Rejected!',
        'Silahkan cek kembali data dan import dokumen kembali !!',
        [
          {
            text: 'Cancel',
            onPress: () => null,
            style: 'cancel',
          },
          {text: 'Import', onPress: () => onImportDoc(data)},
        ],
      );
      return;
    }

    navigation.navigate('PemilihScreen', {id_document: data.id});
  };
  const onExport = async (data) => {
    setLoadingExport(true);

    const docColl = firestore().collection('document');

    await docColl
      .doc(data.id)
      .get()
      .then((snap) => {
        const dataList = [];

        const datafetch = snap.data() || null;
        if (datafetch?.pemilih?.length > 0) {
          datafetch.pemilih.forEach((pemilih) => {
            if (pemilih) {
              dataList.push({...pemilih});
            }
          });
          exportExcel(dataList, `${data.created_name}_${data.upload_date}`);
          setLoadingExport(false);
        }
      })
      .catch((e) => {
        setLoadingExport(false);
        console.log('e', e);
      });
  };

  const removeDocument = (doc) => {
    const docDb = firestore().collection('document');
    docDb.doc(doc.id).delete();
  };

  const alertRmove = (doc) => {
    Alert.alert(
      'Hapus Dokumen!',
      'Apakah Kamu yakin ingin menghapus dokumen ini ?',
      [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'YES', onPress: () => onRemove(doc)},
      ],
    );
  };

  const onRemove = async (doc) => {
    removeDocument(doc);
  };

  const initialScreen = async () => {
    setLoading(true);
    let userCollection = firestore()
      .collection('document')
      .where('ref_id', '!=', 'document_ppdp');

    const unsubscribe = await userCollection
      // .orderBy('createdAt', 'desc')
      .onSnapshot((snapshot) => {
        let list = [];
        if (snapshot) {
          snapshot.forEach((doc) => {
            const localDate = doc.data().createdAt
              ? doc.data().createdAt?.toDate()
              : null;
            const day = localDate?.getDate();
            const month = localDate?.getMonth() + 1; // getMonth() returns month from 0 to 11
            const year = localDate?.getFullYear();

            const timeAsString = day + '-' + month + '-' + year;

            list.push({
              id: doc.id,
              created_by: doc.data().createdBy,
              created_name: doc.data().createdName,
              ref_id: doc.data().ref_id,
              created_at_timestamp: doc.data().createdAt,
              createdAt: doc.data().createdAt?.toDate(),
              upload_date: timeAsString,
              total_doc: doc.data().total_doc || 0,
              status_key: doc.data().status_key,
            });
          });
          setDocs(list);
        }
      });

    setLoading(false);

    return () => {
      unsubscribe();
    };
  };

  const getStatus = (data) => {
    let result = '';
    switch (data.status_key) {
      case 1:
        result = 'Waiting Response';
        break;
      case 2:
        result = 'Rejected';
        break;
      case 3:
        result = 'Approved';
        break;
      default:
        break;
    }

    return result;
  };

  const getStatusIcon = (data) => {
    let result = '';
    switch (data.status_key) {
      case 1:
        result = 'autorenew';
        break;
      case 2:
        result = 'cancel';
        break;
      case 3:
        result = 'check-circle';
        break;
      default:
        break;
    }

    return result;
  };

  const onChangeFilter = (filter) => {
    updateState({
      status: filter.status,
      start: filter.start,
      end: filter.end,
    });
  };

  const approvedoc = async (doc) => {
    const docdb = firestore().collection('document');
    await docdb
      .doc(doc.id)
      .update({
        status_key: Document_Status.verified,
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        // navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
  };

  const rejectdoc = async (doc) => {
    const docdb = firestore().collection('document');
    await docdb
      .doc(doc.id)
      .update({
        status_key: Document_Status.rejected,
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        // navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
  };

  const onApprove = (data) => {
    Alert.alert('Approve!', 'Dokumen akan di approve, apa kamu yakin ?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {text: 'YES', onPress: async () => await approvedoc(data)},
    ]);
  };

  const onReject = (data) => {
    Alert.alert('Reject!', 'Dokumen akan di reject, apa kamu yakin ?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {text: 'YES', onPress: async () => await rejectdoc(data)},
    ]);
  };

  const getUserData = async () => {
    await AsyncStorage.getItem('userData').then(async (item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        updateState({
          user: user,
        });
      }
    });
  };

  const getDocsFiltered = () => {
    let docfilter = getDocs().sort((a, b) => {
      return b.createdAt > a.createdAt;
    });

    if (state.user?.role == 'ppdp') {
      docfilter = docfilter.filter((doc) => {
        return doc.status_key == Document_Status.verified;
      });
    }

    if (state.status || state.end || state.start) {
      if (state.start && state.end && state.status) {
        const startDate = firestore.Timestamp.fromDate(new Date(state.start));
        const endDate = firestore.Timestamp.fromDate(
          new Date(new Date(state.end).setUTCHours(23, 59, 59, 999)),
        );
        return docfilter.filter((doc) => {
          return (
            doc.status_key == state.status &&
            doc.created_at_timestamp >= startDate &&
            doc.created_at_timestamp <= endDate
          );
        });
      } else if (state.status) {
        return docfilter.filter((doc) => {
          return doc.status_key == state.status;
        });
      } else if (state.start && state.end) {
        const startDate = firestore.Timestamp.fromDate(new Date(state.start));
        const endDate = firestore.Timestamp.fromDate(
          new Date(new Date(state.end).setUTCHours(23, 59, 59, 999)),
        );
        return docfilter.filter((doc) => {
          return (
            doc.created_at_timestamp >= startDate &&
            doc.created_at_timestamp <= endDate
          );
        });
      }
    }
    return docfilter;
  };

  useEffect(() => {
    initialScreen();
  }, [state.status, state.start, state.end]);

  useImperativeHandle(ref, () => ({
    onChangeFilter,
  }));

  useEffect(() => {
    getUserData();
    // initialScreen();
  }, []);

  if (isLoading()) {
    return <ActivityIndicator size={'large'} color={colors.black} />;
  }

  return (
    <View>
      {getDocsFiltered().map((data, index) => (
        <TouchableOpacity key={index} onPress={() => onOpenDoc(data)}>
          <ReCardView
            style={{
              marginBottom: hp(2),
              borderRadius: wp(1.5),
              padding: wp(0.5),
              // backgroundColor: colors.primary,
            }}>
            <LinearGradient
              start={{x: 0.0, y: 0.0}}
              end={{x: 0.9, y: 0.9}}
              colors={[colors.secondary, colors.white]}
              style={{
                flexDirection: 'row',
                borderRadius: wp(1),
                // height: hp(10),
                alignItems: 'center',
                paddingHorizontal: wp(3),
                paddingVertical: hp(2),
              }}>
              <View style={{flex: 1, marginRight: wp(1)}}>
                <Image
                  source={require('../../res/images/documents.png')}
                  style={{
                    height: hp(6),
                    width: hp(6),
                    borderRadius: hp(7),
                    resizeMode: 'cover',
                  }}
                />
              </View>
              <View style={{flex: 6, marginLeft: wp(2)}}>
                <View
                  style={{
                    flexDirection: 'row',
                    marginBottom: hp(0.5),
                  }}>
                  <FontAweSome
                    style={{
                      textAlignVertical: 'center',
                      marginTop: hp(-0.2),
                      marginRight: wp(1.5),
                    }}
                    name={'calendar-o'}
                    color={colors.textSecondary}
                    size={hp(1.8)}
                  />
                  <Text
                    style={{
                      fontSize: wp(3.2),
                      color: colors.textGray,
                      fontFamily: 'Poppins-Regular',
                    }}>
                    {data.upload_date}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginBottom: hp(0.5),
                  }}>
                  <FontAweSome
                    style={{
                      textAlignVertical: 'center',
                      marginTop: hp(-0.2),
                      marginRight: wp(1.9),
                    }}
                    name={'user'}
                    color={colors.textSecondary}
                    size={hp(1.8)}
                  />
                  <Text
                    style={{
                      fontSize: wp(3.2),
                      color: colors.textGray,
                      fontFamily: 'Poppins-Regular',
                    }}>
                    {data.created_name}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginBottom: hp(0.5),
                  }}>
                  <FontAweSome
                    style={{
                      textAlignVertical: 'center',
                      marginTop: hp(-0.2),
                      marginRight: wp(1.5),
                    }}
                    name={'list-alt'}
                    color={colors.textSecondary}
                    size={hp(1.8)}
                  />
                  <Text
                    style={{
                      fontSize: wp(3.2),
                      color: colors.textGray,
                      fontFamily: 'Poppins-Regular',
                    }}>
                    {`${data.total_doc} data pemilih`}
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                  }}>
                  <MaterialIcon
                    style={{
                      textAlignVertical: 'center',
                      marginTop: hp(-0.2),
                      marginRight: wp(1.5),
                    }}
                    name={getStatusIcon(data)}
                    color={colors.textSecondary}
                    size={hp(1.8)}
                  />
                  <Text
                    style={{
                      fontSize: wp(3.2),
                      color: colors.textGray,
                      fontFamily: 'Poppins-Regular',
                    }}>
                    {getStatus(data)}
                  </Text>
                </View>
              </View>
              <View style={{flex: 0.8}}>
                <TouchableOpacity
                  style={{alignItems: 'center', flex: 1}}
                  onPress={() => onExport(data)}>
                  <FontAweSome
                    name="download"
                    color={colors.secondary}
                    size={hp(3.4)}
                  />
                  {/* <Text style={{fontSize: wp(2.7), marginBottom: hp(1)}}>
                    Export
                  </Text> */}
                </TouchableOpacity>
                {validationType && (
                  <TouchableOpacity
                    style={{
                      alignItems: 'center',
                      flex: 1,
                      justifyContent: 'flex-end',
                    }}
                    onPress={() => onApprove(data)}>
                    <FontAweSome
                      name="check"
                      color={colors.greenDefault}
                      size={hp(3.5)}
                    />
                  </TouchableOpacity>
                )}
                {validationType && (
                  <TouchableOpacity
                    style={{
                      alignItems: 'center',
                      flex: 1,
                      justifyContent: 'flex-end',
                    }}
                    onPress={() => onReject(data)}>
                    <FontAweSome
                      name="close"
                      color={colors.darkOrange}
                      size={hp(3.5)}
                    />
                  </TouchableOpacity>
                )}
                {!validationType &&
                  ['admin', 'kpu'].includes(state.user?.role) && (
                    <TouchableOpacity
                      style={{
                        alignItems: 'center',
                        flex: 1,
                        justifyContent: 'flex-end',
                      }}
                      onPress={() => alertRmove(data)}>
                      <FontAweSome
                        name="trash"
                        color={colors.darkOrange}
                        size={hp(3.4)}
                      />
                    </TouchableOpacity>
                  )}
              </View>
            </LinearGradient>
          </ReCardView>
        </TouchableOpacity>
      ))}
      <ReLoadingModal visible={state.loadingexprt} />
    </View>
  );
};

export default forwardRef(ListDocumentData);
