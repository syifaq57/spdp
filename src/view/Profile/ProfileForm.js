/* eslint-disable no-alert */
import React, {useState, useRef, useImperativeHandle, forwardRef} from 'react';

import {View, Text} from 'react-native';

import {Container} from 'native-base';
import DeafaultHeader from '../../component/DeafaultHeader';
import colors from '../../res/colors';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import ReCardView from '../../component/ReCardView';
import DefaultContainer from '../../component/DefaultContainer';
import ReTextInput from '../../component/ReTextInput';
import RePicker from '../../component/RePicker';
import ReButton from '../../component/ReButton';
import ReImagePicker from '../../component/ReImagePicker';
import LoadingIndicator from '../../component/LoadingIndicator';
import ImageUploader from '../../handler/ImageUploader';

import firestore from '@react-native-firebase/firestore';
import {useNavigation} from '@react-navigation/native';
import ReLoadingModal from '../../component/ReLoadingModal';

const initialState = {
  id: '',
  nama: '',
  role: '',
  username: '',
  password: '',
  image: '',
  display: false,
  loadingModal: false,
};

const roleList = [
  {
    value: 'admin',
    label: 'Admin',
  },
  {
    value: 'ppdp',
    label: 'PPDP',
  },
  {
    value: 'kpu',
    label: 'KPU',
  },
  {
    value: 'pps',
    label: 'PPS',
  },
];

const ProfileForm = ({roleEditable}, ref) => {
  const [state, setState] = useState(initialState);

  const navigation = useNavigation();
  const reImagePicker = useRef(null);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const setLoading = (flag) => {
    updateState({
      loadingModal: flag,
    });
  };

  const setUser = (user) => {
    if (!user) {
      updateState({
        display: true,
      });
      return;
    }

    user = {
      ...user,
    };

    updateState({
      id: user.id,
      nama: user.nama,
      image: user.image,
      username: user.username,
      role: user.role,
      password: user.password,
      display: true,
    });
  };

  const updateUser = async (newData) => {
    const userCollection = firestore().collection('user');
    await userCollection
      .doc(state.id)
      .update({
        nama: newData.nama,
        username: newData.username,
        password: newData.password,
        role: newData.role,
        image: newData.image,
        createdAt: newData.createdAt,
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        navigation.goBack();

        setState(initialState);
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
  };

  const createUser = async (newData) => {
    const userCollection = firestore().collection('user');

    await userCollection
      .add({
        nama: newData.nama,
        username: newData.username,
        password: newData.password,
        role: newData.role,
        image: newData.image,
        createdAt: newData.createdAt,
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        setState(initialState);
        navigation.goBack();
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });
  };

  const preparedData = async () => {
    const image = reImagePicker.current?.getImage();

    const fotoUri = image.imageObject
      ? await ImageUploader(image.image)
      : state.image;

    return {
      nama: state.nama,
      username: state.username,
      password: state.password,
      role: state.role,
      image: fotoUri,
      createdAt: firestore.FieldValue.serverTimestamp(),
    };
  };

  const onSaveProfile = async () => {
    setLoading(true);
    const upsertdata = await preparedData();
    if (state.id) {
      await updateUser(upsertdata);
    } else {
      await createUser(upsertdata);
    }

    setLoading(false);
  };

  useImperativeHandle(ref, () => ({
    setUser,
  }));

  if (!state.display) {
    return <LoadingIndicator />;
  }

  return (
    <View>
      <ReImagePicker
        ref={reImagePicker}
        defaultImage={state.image}
        style={{marginBottom: hp(0.5)}}
      />
      <ReTextInput
        label="Nama"
        value={state.nama}
        onChangeText={(val) => {
          updateState({nama: val});
        }}
        style={{marginBottom: hp(1.2)}}
      />

      <RePicker
        label="Role"
        enabled={roleEditable ? true : false}
        items={roleList}
        selectedValue={state.role}
        style={{marginBottom: hp(1.2)}}
        onValueChange={(value) => {
          updateState({
            role: value,
          });
        }}
      />
      <ReTextInput
        label="Username"
        value={state.username}
        onChangeText={(val) => {
          updateState({username: val});
        }}
        style={{marginBottom: hp(1.2)}}
      />
      <ReTextInput
        label="Password"
        value={state.password}
        onChangeText={(val) => {
          updateState({password: val});
        }}
        passwordInput
        style={{marginBottom: hp(1)}}
      />
      <ReButton
        style={{marginHorizontal: wp(8), marginVertical: hp(4.5)}}
        label="SAVE"
        disabled={false}
        onPress={onSaveProfile}
      />
      <ReLoadingModal
        visible={state.loadingModal}
        title="Menyimpan Data User.."
      />
    </View>
  );
};

export default forwardRef(ProfileForm);
