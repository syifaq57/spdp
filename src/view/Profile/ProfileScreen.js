/* eslint-disable react-native/no-inline-styles */
import React, {useState, useRef, useEffect} from 'react';

import {View} from 'react-native';

import DeafaultHeader from '../../component/DeafaultHeader';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import DefaultContainer from '../../component/DefaultContainer';
import ProfileForm from './ProfileForm';

import firestore from '@react-native-firebase/firestore';
import {useRoute} from '@react-navigation/native';

const initialState = {
  nama: '',
};

const ProfileScreen = () => {
  const [state, setState] = useState(initialState);

  const route = useRoute();

  const profileForm = useRef(null);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const loadUser = () => {
    return firestore().collection('user').doc('ya7QUh5kGpOh2w7pirjQ').get();
  };
  const initialScreen = async () => {
    const userParam = route?.params?.user;
    if (userParam) {
      updateState({
        editMode: true,
      });
    }
    profileForm.current?.setUser(userParam);
  };

  useEffect(() => {
    initialScreen();
  }, []);

  return (
    <DefaultContainer>
      <DeafaultHeader title="PROFILE" backButton />
      <View style={{flex: 1, paddingHorizontal: wp(8), paddingTop: hp(2)}}>
        <ProfileForm ref={profileForm} />
      </View>
    </DefaultContainer>
  );
};

export default ProfileScreen;
