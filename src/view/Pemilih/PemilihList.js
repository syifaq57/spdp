/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {Container, Content, Card} from 'native-base';

import colors from '../../res/colors';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';

import firestore from '@react-native-firebase/firestore';

import {useNavigation, useRoute} from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ReCardView from '../../component/ReCardView';
import {TextInput} from 'react-native-gesture-handler';
import ReSearchInput from '../../component/ReSearchInput';
import IconPicker from '../../component/IconPicker';
import RePicker from '../../component/RePicker';
import PemilihDetail from './PemilihDetail';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Document_Status} from '../../value/constant';

const initialState = {
  loading: false,
  pemilihList: [],
  search: '',
  searchBy: 'NAMA',
};

const bys = [
  {label: 'Name', value: 'NAMA'},
  {label: 'NIK', value: 'NIK'},
  {label: 'TPS', value: 'TPS'},
];

const PemilihList = () => {
  const route = useRoute();

  const [state, setState] = useState(initialState);

  const navigation = useNavigation();

  const pemilihDetail = useRef(null);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const isLoading = () => {
    return state.loading;
  };

  const setLoading = (flag) => {
    updateState({
      loading: flag,
    });
  };

  const getPemilihList = () => {
    let pemilih = state.pemilihList;

    if (state.search) {
      pemilih = pemilih.filter((eachItem) => {
        return eachItem[state.searchBy]
          .toLowerCase()
          .includes(state.search.toLowerCase());
      });
    }

    return pemilih;
  };

  const setPemilihList = (val) => {
    updateState({
      pemilihList: val,
    });
  };

  const onOpenPemilih = (pemilih) => {
    pemilihDetail.current?.show(pemilih);
  };

  const editPemilih = (pemilih) => {
    navigation.navigate('FormPemilih', {pemilih: pemilih});
  };

  const onSearchText = (value) => {
    updateState({
      search: value?.toString(),
    });
  };

  const onChangeSerachBy = (by) => {
    if (!by || by == state.searchBy) {
      return;
    }

    updateState({
      searchBy: by,
      search: '',
    });
  };

  let unsub;

  const initialScreen = async () => {
    setLoading(true);

    const ref_id = route?.params?.id_document;

    if (ref_id) {
      if (unsub) {
        unsub();
      }

      let docColl = firestore().collection('document');
      unsub = await docColl
        .doc(ref_id)
        .get()
        .then((snap) => {
          const dataList = [];

          const datafetch = snap.data() || null;

          if (datafetch?.pemilih?.length > 0) {
            datafetch.pemilih.forEach((pemilih) => {
              if (pemilih) {
                dataList.push({...pemilih});
              }
            });
          }

          setPemilihList(dataList);
        })
        .catch((e) => {
          console.log('e list pemilih', e);
        });

      if (unsub) {
        unsub();
      }
    } else {
      if (unsub) {
        unsub();
      }

      const useData = await AsyncStorage.getItem('userData');
      let filterppdp = false;
      if (useData) {
        const data = JSON.parse(useData);
        if (data && Object.keys(data).length > 0) {
          if (data.role == 'ppdp') {
            filterppdp = true;
          }
        }
      }
      let docColl = firestore().collection('document');
      if (filterppdp) {
        docColl = docColl.where('status_key', '==', Document_Status.verified);
      }
      unsub = await docColl.get().then((snap) => {
        const dataList = [];

        snap.forEach((doc) => {
          const docFetch = doc.data();

          if (docFetch?.pemilih.length > 0) {
            docFetch.pemilih.forEach((pemilih) => {
              if (pemilih) {
                dataList.push({...pemilih});
              }
            });
          }

          setPemilihList(dataList);
        });
      });

      if (unsub) {
        unsub();
      }
    }

    setLoading(false);
  };

  const getListFiltered = () => {
    let list = getPemilihList();
    if (!state.search || state.search.length > 0) {
      return list;
    }

    return list.filter((item) => {
      return item[state.searchBy].includes(state.search);
    });
  };

  // useEffect(() => {
  //   initialScreen();
  // }, []);

  useEffect(() => {
    return navigation.addListener('focus', () => {
      initialScreen();
    });
  }, [navigation]);

  if (isLoading()) {
    return <ActivityIndicator size={'large'} color={colors.black} />;
  }

  return (
    <View>
      <View
        style={{marginTop: hp(0.1), flexDirection: 'row', marginBottom: hp(2)}}>
        <ReSearchInput
          style={{flex: 1}}
          placeholder={`Search by ${state.searchBy}`}
          value={state.search}
          onChangeText={onSearchText}
        />

        <IconPicker
          style={{marginLeft: wp(2)}}
          items={bys}
          onValueChange={onChangeSerachBy}
        />
      </View>
      <ScrollView>
        {getListFiltered().map((data, index) => (
          <TouchableOpacity key={index} onPress={() => onOpenPemilih(data)}>
            <ReCardView
              style={{
                marginBottom: hp(2),
                borderRadius: wp(1.5),
                padding: wp(0.5),
                // backgroundColor: colors.primary,
              }}>
              <LinearGradient
                start={{x: 0.0, y: 0.0}}
                end={{x: 0.9, y: 0.9}}
                colors={[colors.secondary, colors.white]}
                style={{
                  flexDirection: 'row',
                  borderRadius: wp(1),
                  height: hp(11),
                  alignItems: 'center',
                  paddingHorizontal: wp(3),
                }}>
                <View style={{flex: 1, marginRight: wp(1)}}>
                  <Image
                    source={require('../../res/images/Profile.png')}
                    style={{
                      height: hp(6),
                      width: hp(6),
                      borderRadius: hp(7),
                      resizeMode: 'cover',
                    }}
                  />
                </View>
                <View style={{flex: 6, marginLeft: wp(2)}}>
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={{
                      color: colors.textGray,
                      fontSize: wp(3),
                      fontFamily: 'JosefinSans-Bold',
                      marginBottom: hp(0.5),
                    }}>
                    {data.NAMA}
                  </Text>
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={{
                      color: colors.textGray,
                      fontSize: wp(2.8),
                      fontFamily: 'JosefinSans-Medium',
                      marginBottom: hp(0.5),
                    }}>
                    {data.NIK}
                  </Text>
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={{
                      color: colors.textGray,
                      fontSize: wp(2.8),
                      fontFamily: 'JosefinSans-Medium',
                      marginBottom: hp(0.5),
                    }}>
                    {`TPS : ${data.TPS}`}
                  </Text>
                </View>
                <View style={{flex: 0.5}}>
                  <TouchableOpacity onPress={() => editPemilih(data)}>
                    <FontAweSome
                      name="pencil"
                      color={colors.secondary}
                      size={hp(3.2)}
                    />
                  </TouchableOpacity>
                </View>
              </LinearGradient>
            </ReCardView>
          </TouchableOpacity>
        ))}
        <View style={{height: 200}} />
      </ScrollView>

      <PemilihDetail ref={pemilihDetail} />
    </View>
  );
};

export default PemilihList;
