/* eslint-disable react-native/no-inline-styles */
import React, {useState, useRef, useEffect} from 'react';

import {View, ScrollView} from 'react-native';

import DeafaultHeader from '../../component/DeafaultHeader';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import DefaultContainer from '../../component/DefaultContainer';
// import ProfileForm from '../Profile/ProfileForm';

import firestore from '@react-native-firebase/firestore';
import {useRoute} from '@react-navigation/native';
import ReTextInput from '../../component/ReTextInput';
import ReButton from '../../component/ReButton';

import {useNavigation} from '@react-navigation/native';

const initialState = {};

const FormPemilih = () => {
  const [state, setState] = useState(initialState);

  const navigation = useNavigation();

  const profileForm = useRef(null);
  const route = useRoute();

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const setLoading = (flag) => {
    updateState({
      loading: flag,
    });
  };

  const initialScreen = async () => {
    const pemilihParam = route?.params?.pemilih;
    if (pemilihParam) {
      updateState({
        ...pemilihParam,
      });
    }
  };

  const onSavePemilih = async () => {
    const newData = {
      ...state,
    };

    // delete newData.id;

    const userCollection = firestore().collection('document');
    let oldpemilih = null;
    await userCollection
      .doc(newData.id_doc_ref)
      .get()
      .then((data) => {
        oldpemilih = data.data().pemilih;
      })
      .catch((e) => {
        console.log('e', e);
      });

    if (oldpemilih) {
      const indexP = oldpemilih.findIndex((x) => {
        return x._id == newData._id;
      });
      if (indexP > -1) {
        oldpemilih[indexP] = newData;
      }

      await userCollection
        .doc(newData.id_doc_ref)
        .update({
          pemilih: oldpemilih,
        })
        .then(() => {
          alert('Berhasil menyimpan data');
          navigation.goBack();
          navigation.state.params.setLoading(true);
          setState(initialState);
        })
        .catch((e) => {
          console.log('e', e);
        });
    }
  };

  useEffect(() => {
    initialScreen();
  }, []);

  return (
    <DefaultContainer>
      <DeafaultHeader title={'Edit Data Pemilih'} backButton />
      <ScrollView
        style={{
          flex: 1,
        }}>
        <View style={{flex: 1, paddingHorizontal: wp(8), paddingTop: hp(2)}}>
          <ReTextInput
            label="Nama"
            value={state.NAMA}
            onChangeText={(val) => {
              updateState({NAMA: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="Alamat"
            value={state.ALAMAT}
            onChangeText={(val) => {
              updateState({ALAMAT: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="RT"
            value={state.RT}
            onChangeText={(val) => {
              updateState({RT: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="RW"
            value={state.RW}
            onChangeText={(val) => {
              updateState({RW: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />

          <ReTextInput
            label="Sumber Data"
            value={state.SUMBERDATA}
            onChangeText={(val) => {
              updateState({SUMBERDATA: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="TPS"
            value={state.TPS}
            onChangeText={(val) => {
              updateState({TPS: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReButton
            style={{marginHorizontal: wp(8), marginVertical: hp(4.5)}}
            label="SAVE"
            disabled={false}
            onPress={() => {
              onSavePemilih();
            }}
          />
        </View>
      </ScrollView>
    </DefaultContainer>
  );
};

export default FormPemilih;
