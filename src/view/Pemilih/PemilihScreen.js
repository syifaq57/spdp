/* eslint-disable react-native/no-inline-styles */
import React, {useState, useRef, useEffect} from 'react';

import {View} from 'react-native';

import DeafaultHeader from '../../component/DeafaultHeader';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import DefaultContainer from '../../component/DefaultContainer';
import ProfileForm from '../Profile/ProfileForm';

import firestore from '@react-native-firebase/firestore';
import DefaultContentView from '../../component/DefaultContentView';
import {useNavigation} from '@react-navigation/native';
import PemilihList from './PemilihList';
import AsyncStorage from '@react-native-async-storage/async-storage';

const initialState = {
  nama: '',
};
const PemilihScreen = () => {
  const navigation = useNavigation();

  const [isHeaderppdp, setHeaderpdpp] = useState(false);

  useEffect(() => {
    initialScreen();
  }, []);

  const initialScreen = async () => {
    const useData = await AsyncStorage.getItem('userData');
    let headerppdp = false;
    if (useData) {
      const data = JSON.parse(useData);
      if (data && Object.keys(data).length > 0) {
        if (data.role == 'ppdp') {
          headerppdp = true;
        }
      }
    }

    setHeaderpdpp(headerppdp);
  };

  const renderHeader = () => {
    if (isHeaderppdp) {
      return (
        <DeafaultHeader
          title="Data Pemilih"
          backButton
          rightButton={'plus'}
          onRightButtonPress={() => {
            navigation.navigate('TambahPemilih');
          }}
        />
      );
    } else {
      return <DeafaultHeader title="Data Pemilih" backButton />;
    }
  };
  return (
    <DefaultContainer>
      {renderHeader()}
      <DefaultContentView>
        <PemilihList />
      </DefaultContentView>
    </DefaultContainer>
  );
};

export default PemilihScreen;
