/* eslint-disable react-native/no-inline-styles */
import React, {useState, useRef, useEffect} from 'react';

import {View, ScrollView} from 'react-native';

import DeafaultHeader from '../../component/DeafaultHeader';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import DefaultContainer from '../../component/DefaultContainer';
// import ProfileForm from '../Profile/ProfileForm';

import firestore from '@react-native-firebase/firestore';
import {useRoute} from '@react-navigation/native';
import ReTextInput from '../../component/ReTextInput';
import ReButton from '../../component/ReButton';

import {useNavigation} from '@react-navigation/native';
import uuid from 'react-native-uuid';

const initialState = {
  ALAMAT: '',
  DIFABEL: '',
  EKTP: '',
  ID: '',
  JENIS_KELAMIN: '',
  KAWIN: '',
  KET: '',
  KODE_KAB: '',
  NAMA: '',
  NAMA_KAB: '',
  NAMA_KEC: '',
  NAMA_KEL: '',
  NAMA_PRO: '',
  NIK: '',
  NKK: '',
  NO_AKTA_MATI: '',
  NO_AKTA_TGL: '',
  RT: '',
  RW: '',
  SUMBERDATA: '',
  TANGGAL_LAHIR: '',
  TEMPAT_LAHIR: '',
  TGL_MATI: '',
  TPS: '',
};

const TambahPemilih = () => {
  const [state, setState] = useState(initialState);

  const [loading, setLoading] = useState(false);

  const navigation = useNavigation();

  const profileForm = useRef(null);
  const route = useRoute();

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const initialScreen = async () => {
    const pemilihParam = route?.params?.pemilih;
    if (pemilihParam) {
      updateState({
        ...pemilihParam,
      });
    }
  };

  const onSavePemilih = async () => {
    const newData = {
      ...state,
    };

    // delete newData.id;

    const userCollection = firestore().collection('document');
    let oldpemilih = null;
    await userCollection
      .doc(newData.id_doc_ref)
      .get()
      .then((data) => {
        oldpemilih = data.data().pemilih;
      })
      .catch((e) => {
        console.log('e', e);
      });

    if (oldpemilih) {
      const indexP = oldpemilih.findIndex((x) => {
        return x._id == newData._id;
      });
      if (indexP > -1) {
        oldpemilih[indexP] = newData;
      }

      await userCollection
        .doc(newData.id_doc_ref)
        .update({
          pemilih: oldpemilih,
        })
        .then(() => {
          alert('Berhasil menyimpan data');
          navigation.goBack();
          setState(initialState);
        })
        .catch((e) => {
          console.log('e', e);
        });
    }
  };

  const getPpdpDoc = async () => {
    const documentCollection = firestore().collection('document');

    let document = null;

    await documentCollection
      .doc('document_ppdp')
      .get()
      .then((result) => {
        document = {
          ...result.data(),
        };
      })
      .catch((e) => {
        console.log('erorr', e);
      });

    return document;
  };

  const onCreatePemilih = async () => {
    setLoading(true);
    const documentCollection = firestore().collection('document');

    const ppdDoc = await getPpdpDoc();

    const pemilihs = ppdDoc.pemilih.concat({
      ...state,
      id_doc_ref: ppdDoc.ref_id,
      _id: uuid.v1(),
    });

    await documentCollection
      .doc('document_ppdp')
      .update({
        total_doc: pemilihs.length,
        pemilih: pemilihs,
      })
      .then(() => {
        alert('Berhasil menyimpan data');
        navigation.goBack();
        setState(initialState);
      })
      .catch(() => {
        alert('Gagal menyimpan data');
      });

    setLoading(false);
  };
  useEffect(() => {
    initialScreen();
  }, []);

  return (
    <DefaultContainer>
      <DeafaultHeader title={'Tambah Data Pemilih'} backButton />
      <ScrollView
        style={{
          flex: 1,
        }}>
        <View style={{flex: 1, paddingHorizontal: wp(8), paddingTop: hp(2)}}>
          <ReTextInput
            label="ID"
            value={state.ID}
            onChangeText={(val) => {
              updateState({ID: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="e-KTP"
            value={state.EKTP}
            onChangeText={(val) => {
              updateState({EKTP: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="NIK"
            value={state.NIK}
            onChangeText={(val) => {
              updateState({NIK: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="NKK"
            value={state.NKK}
            onChangeText={(val) => {
              updateState({NKK: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="Nama"
            value={state.NAMA}
            onChangeText={(val) => {
              updateState({NAMA: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="Tempat Lahir"
            value={state.TEMPAT_LAHIR}
            onChangeText={(val) => {
              updateState({TEMPAT_LAHIR: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="Tanggal Lahir"
            value={state.TANGGAL_LAHIR}
            onChangeText={(val) => {
              updateState({TANGGAL_LAHIR: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />

          <ReTextInput
            label="Jenis Kelamin"
            value={state.JENIS_KELAMIN}
            onChangeText={(val) => {
              updateState({JENIS_KELAMIN: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="Kawin"
            value={state.KAWIN}
            onChangeText={(val) => {
              updateState({KAWIN: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="Alamat"
            value={state.ALAMAT}
            onChangeText={(val) => {
              updateState({ALAMAT: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="RT"
            value={state.RT}
            onChangeText={(val) => {
              updateState({RT: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="RW"
            value={state.RW}
            onChangeText={(val) => {
              updateState({RW: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="Sumber Data"
            value={state.SUMBERDATA}
            onChangeText={(val) => {
              updateState({SUMBERDATA: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="TPS"
            value={state.TPS}
            onChangeText={(val) => {
              updateState({TPS: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />

          <ReTextInput
            label="Kode Kabupaten"
            value={state.KODE_KAB}
            onChangeText={(val) => {
              updateState({KODE_KAB: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="NAM Kabupaten"
            value={state.NAMA_KAB}
            onChangeText={(val) => {
              updateState({NAMA_KAB: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="Kode Kecamatan"
            value={state.NAMA_KEC}
            onChangeText={(val) => {
              updateState({NAMA_KEC: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="Nama Kelurahan"
            value={state.NAMA_KEL}
            onChangeText={(val) => {
              updateState({NAMA_KEL: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="Nama Provinsi"
            value={state.NAMA_PRO}
            onChangeText={(val) => {
              updateState({NAMA_PRO: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="Difabel"
            value={state.DIFABEL}
            onChangeText={(val) => {
              updateState({DIFABEL: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />

          <ReTextInput
            label="No. Akta Mati"
            value={state.NO_AKTA_MATI}
            onChangeText={(val) => {
              updateState({NO_AKTA_MATI: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReTextInput
            label="No. Akta Tanggal"
            value={state.NO_AKTA_TGL}
            onChangeText={(val) => {
              updateState({NO_AKTA_TGL: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />

          <ReTextInput
            label="Tanggal Mati"
            value={state.TGL_MATI}
            onChangeText={(val) => {
              updateState({TGL_MATI: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />

          <ReTextInput
            label="Keterangan"
            value={state.KET}
            onChangeText={(val) => {
              updateState({KET: val});
            }}
            style={{marginBottom: hp(1.2)}}
          />
          <ReButton
            style={{marginHorizontal: wp(8), marginVertical: hp(4.5)}}
            label="SAVE"
            disabled={false}
            onPress={() => {
              onCreatePemilih();
            }}
          />
        </View>
      </ScrollView>
    </DefaultContainer>
  );
};

export default TambahPemilih;
