/* eslint-disable react-native/no-inline-styles */
import {View, Text} from 'react-native';
import React, {forwardRef, useImperativeHandle, useState} from 'react';

import Modal from 'react-native-modal';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';
import {ScrollView} from 'react-native-gesture-handler';

const PemilihDetail = ({}, ref) => {
  const [pemilih, setPemilih] = useState(null);

  const show = (val) => {
    setPemilih(val);
  };

  const isVisible = () => {
    return pemilih ? true : false;
  };

  useImperativeHandle(ref, () => ({show}));

  const detailitem = (title, value) => {
    return (
      <View style={{marginBottom: hp(1)}}>
        <Text
          style={{
            fontSize: wp(3.5),
            color: colors.textGray,
            fontFamily: 'Poppins-Light',
          }}>
          {title}
        </Text>
        <Text
          style={{
            fontSize: wp(4.5),
            marginLeft: wp(1),

            fontFamily: 'Poppins-Regular',
          }}>
          {value}
        </Text>
      </View>
    );
  };

  if (!pemilih) {
    return null;
  }

  return (
    <View>
      <Modal
        style={{alignItems: 'center'}}
        backdropOpacity={0.4}
        animationIn={'fadeIn'}
        animationOut={'fadeOut'}
        onBackdropPress={() => setPemilih(null)}
        onBackButtonPress={() => setPemilih(null)}
        isVisible={isVisible()}>
        <View
          style={{
            marginTop: hp(-5),
            backgroundColor: colors.white,
            width: '95%',
            height: '90%',
            borderRadius: wp(2),
          }}>
          <View
            style={{
              borderBottomWidth: 1,
              backgroundColor: colors.primary,
              height: wp(15),
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: wp(5),
                color: colors.textSecondary,
                fontWeight: 'bold',
              }}>
              Detail Pemilih
            </Text>
          </View>
          <ScrollView
            style={{
              paddingHorizontal: wp(4),
              paddingTop: hp(1),
              paddingBottom: hp(3),
            }}>
            {detailitem('NIK', pemilih.NIK)}
            {detailitem('NKK', pemilih.NKK)}
            {detailitem('Nama', pemilih.NAMA)}

            {detailitem(
              'Jenis Kelamin',
              pemilih.JENIS_KELAMIN == 'L' ? 'Laki-Laki' : 'Perempuan',
            )}
            {detailitem(
              'Tempat / Tanggal Lahir',
              `${pemilih.TEMPAT_LAHIR}, ${pemilih.TANGGAL_LAHIR}`,
            )}
            {detailitem('Alamat', pemilih.ALAMAT)}
            {detailitem('Kelurahan', pemilih.NAMA_KEL)}
            {detailitem('Kecamatan', pemilih.NAMA_KEC)}
            {detailitem('Kabupaten', pemilih.NAMA_KAB)}
            {detailitem('Provinsi', pemilih.NAMA_PRO)}
            {detailitem('TPS', pemilih.TPS)}
            {detailitem('Sumber data', pemilih.SUMBERDATA)}
            {detailitem('Keterangan', pemilih.KET)}
            <View style={{height: 50}} />
          </ScrollView>
        </View>
      </Modal>
    </View>
  );
};

export default forwardRef(PemilihDetail);
