/* eslint-disable react-native/no-inline-styles */
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/native';
import React, {useState, forwardRef, useImperativeHandle} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';

const LogoutModal = ({...props}, ref) => {
  useNavigation;
  const navigation = useNavigation();

  const [visible, setVisible] = useState(false);

  const show = (flag) => {
    setVisible(flag);
  };

  const onLogout = async () => {
    await AsyncStorage.clear();
    navigation.reset({
      index: 0,
      routes: [{name: 'LoginScreen'}],
    });
  };

  useImperativeHandle(ref, () => ({
    show,
  }));

  return (
    <View>
      <Modal
        style={{alignItems: 'center'}}
        backdropOpacity={0.4}
        animationIn={'fadeIn'}
        animationOut={'fadeOut'}
        onBackdropPress={() => setVisible(false)}
        isVisible={visible}>
        <View
          style={{
            backgroundColor: 'white',
            borderRadius: wp('2%'),
            padding: hp(2),
            paddingBottom: wp(4),
            width: wp(85),
            top: hp(-5),
          }}>
          <View
            style={{
              paddingBottom: hp('1%'),
              borderBottomWidth: 0.5,
              borderColor: 'gray',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: wp(4), fontWeight: 'bold'}}>
              Logout User
            </Text>
          </View>
          <View
            style={{
              paddingVertical: hp(4.5),
              marginTop: hp(-0.5),
              alignItems: 'center',
            }}>
            <Text style={{fontSize: wp(3.5)}}>
              Apakah anda yakin ingin Logout ?
            </Text>
          </View>
          <View
            style={{
              alignItems: 'center',
              marginBottom: hp(1),
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              style={{
                flex: 1,
                marginRight: wp(5),

                borderRadius: wp(1),
                alignItems: 'center',
                justifyContent: 'center',
                // width: wp(16),
                height: wp(8),
                // borderWidth: 1,
                backgroundColor: colors.accent,
              }}
              onPress={() => show(false)}>
              <Text
                style={{
                  fontSize: wp('3%'),
                  color: colors.white,
                  fontWeight: 'bold',
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flex: 1,
                marginLeft: wp(5),
                borderRadius: wp(1),
                alignItems: 'center',
                justifyContent: 'center',
                width: wp(16),
                height: wp(8),
                // borderWidth: 1,
                backgroundColor: colors.secondary,
              }}
              onPress={onLogout}>
              <Text
                style={{
                  fontSize: wp('3%'),
                  color: colors.white,
                  fontWeight: 'bold',
                }}>
                Logout
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default forwardRef(LogoutModal);
