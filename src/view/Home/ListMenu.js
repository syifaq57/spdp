/* eslint-disable react-native/no-inline-styles */
import React from 'react';

import {View, Text, Image, TouchableOpacity} from 'react-native';

import ReCardView from '../../component/ReCardView';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';

import FontAweSome from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';

const listMenu = [
  {
    key: 0,
    label: 'Import & Export Data',
    image: require('../../res/images/import.png'),
  },
  {
    key: 1,
    label: 'Data Pemilih',
    image: require('../../res/images/followers.png'),
  },
  {
    key: 2,
    label: 'Validasi Data Pemilih',
    image: require('../../res/images/test.png'),
  },
  {
    key: 3,
    label: 'Management User',
    image: require('../../res/images/users.png'),
  },
];

const ListMenu = ({role}) => {
  const navigation = useNavigation();

  const onPressMenu = (key) => {
    switch (key) {
      case 0:
        navigation.navigate('ExportImportScreen');

        break;
      case 1:
        navigation.navigate('PemilihScreen');

        break;
      case 2:
        navigation.navigate('ValidasiScreen');

        break;
      case 3:
        navigation.navigate('UserScreen');
        break;
      default:
        break;
    }
  };

  const getListMenu = () => {
    if (!role) {
      return listMenu;
    }
    switch (role) {
      case 'admin':
        return listMenu;
      // break;
      case 'ppdp':
        return listMenu.filter((menu) => {
          return [0, 1].includes(menu.key);
        });
      case 'pps':
        return listMenu.filter((menu) => {
          return [1, 2].includes(menu.key);
        });
      case 'kpu':
        return listMenu.filter((menu) => {
          return [0, 1].includes(menu.key);
        });

      default:
        break;
    }
  };

  return (
    <View>
      {getListMenu().map((menu, index) => {
        return (
          <TouchableOpacity key={index} onPress={() => onPressMenu(menu.key)}>
            <ReCardView
              style={{
                marginHorizontal: wp(4),
                padding: wp(4),
                marginTop: hp(2),
                borderRadius: wp(3),
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <Image
                source={menu.image}
                style={{
                  height: hp(6),
                  width: hp(6),
                  borderRadius: hp(7),
                  resizeMode: 'cover',
                  marginRight: wp(4),
                }}
              />
              <Text
                style={{
                  flex: 1,
                  fontSize: wp(4.5),
                  fontFamily: 'Poppins-Medium',
                  color: colors.textGray,
                  marginTop: hp(0.2),
                }}>
                {menu.label}
              </Text>
              <FontAweSome
                name="chevron-right"
                color={colors.secondary}
                size={wp(5)}
              />
            </ReCardView>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default ListMenu;
