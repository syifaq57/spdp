/* eslint-disable react-native/no-inline-styles */
import React from 'react';

import {View, Text, Image, TouchableOpacity} from 'react-native';

import ReCardView from '../../component/ReCardView';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';

import FontAweSome from 'react-native-vector-icons/FontAwesome';
import ReButton from '../../component/ReButton';

import {useNavigation} from '@react-navigation/native';

const ProfileComponent = (props) => {
  const user = props.user || null;

  const navigation = useNavigation();

  const getImage = () => {
    return user?.image || null;
  };
  const onEditProfile = () => {
    navigation.navigate('ProfileScreen', {user: user});
  };

  return (
    <ReCardView
      style={{
        marginHorizontal: wp(4),
        paddingVertical: wp(6),
        paddingHorizontal: wp(3),
        marginTop: hp(2),
        borderRadius: wp(3),
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <TouchableOpacity
        onPress={onEditProfile}
        style={{position: 'absolute', top: 15, right: 15}}>
        <FontAweSome name="pencil" color={colors.secondary} size={wp(5)} />
      </TouchableOpacity>
      <Image
        source={
          getImage()
            ? {uri: getImage()}
            : require('../../res/image/Profile.png')
        }
        style={{
          width: wp(20),
          height: wp(20),
          borderRadius: wp(25),
          resizeMode: 'cover',
          marginBottom: hp(1),
        }}
      />
      <Text
        style={{
          fontSize: wp(4.5),
          color: colors.textGray,
          fontFamily: 'Poppins-Bold',
        }}>
        {user.nama || ''}
      </Text>
      <View
        style={{
          flexDirection: 'row',
        }}>
        <FontAweSome
          style={{
            textAlignVertical: 'center',
            marginTop: hp(-0.2),
            marginRight: wp(0.5),
          }}
          name={'gear'}
          color={colors.secondary}
          size={hp(1.8)}
        />
        <Text
          style={{
            fontSize: wp(3.2),
            color: colors.textGray,
            fontFamily: 'Poppins-Regular',
          }}>
          {user.role || ''}
        </Text>
      </View>
    </ReCardView>
  );
};

export default ProfileComponent;
