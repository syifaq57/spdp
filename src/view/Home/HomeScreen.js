/* eslint-disable react-native/no-inline-styles */
import {View, Text, Alert, BackHandler} from 'react-native';
import React, {useState, useEffect, useRef} from 'react';
import {Container} from 'native-base';
import DeafaultHeader from '../../component/DeafaultHeader';
import colors from '../../res/colors';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import ReCardView from '../../component/ReCardView';
import DefaultContainer from '../../component/DefaultContainer';
import ProfileComponent from './ProfileComponent';
import ListMenu from './ListMenu';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LoadingIndicator from '../../component/LoadingIndicator';

import firestore from '@react-native-firebase/firestore';
import LogoutModal from './LogoutModal';
import {useFocusEffect} from '@react-navigation/native';

const initialState = {
  user: null,
};

const HomeScreen = () => {
  const [state, setState] = useState(initialState);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const logoutModal = useRef(null);

  const loadUser = async (user_id) => {
    // cobak pakek realtime data,, realtime changes
    return firestore().collection('user').doc(user_id).get();
  };

  const initialScreen = async () => {
    await AsyncStorage.getItem('userData').then(async (item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        const fetchuser = await loadUser(user.id);

        const dataUser = {
          id: fetchuser.id,
          ...fetchuser.data(),
        };
        updateState({
          user: dataUser,
        });
        await AsyncStorage.setItem('userData', JSON.stringify(dataUser));
      }
    });
  };

  useEffect(() => {
    initialScreen();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        Alert.alert('Exit App!', 'Apakah Kamu yakin ingin keluar Aplikasi?', [
          {
            text: 'Cancel',
            onPress: () => {
              // return true;
            },
            style: 'cancel',
          },
          {
            text: 'YES',
            onPress: () => {
              BackHandler.exitApp();
              // return false;
            },
          },
        ]);
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  return (
    <DefaultContainer>
      <DeafaultHeader
        title="DASHBOARD"
        rightButton={'power-off'}
        onRightButtonPress={() => {
          logoutModal.current?.show(true);
        }}
      />
      {state.user ? (
        <View style={{flex: 1}}>
          <View
            style={{
              position: 'absolute',
              top: 0,
              backgroundColor: colors.secondary,
              height: hp(15),
              width: wp(100),
              borderBottomLeftRadius: wp(8),
              borderBottomRightRadius: wp(8),
            }}
          />
          <ProfileComponent user={state.user} />
          <ListMenu role={state.user?.role} />
        </View>
      ) : (
        <LoadingIndicator />
      )}

      <LogoutModal ref={logoutModal} />
    </DefaultContainer>
  );
};

export default HomeScreen;
