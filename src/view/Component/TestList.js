/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Image, FlatList, Text, TouchableOpacity} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Card} from 'native-base';
import colors from '../../res/colors';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';

import {useNavigation} from '@react-navigation/native';

const TestList = (props) => {
  const navigation = useNavigation();
  const formatData = (data, numColumns) => {
    // if admin
    if (props.role == 'Admin') {
      const plusData = {
        id: '1',
        label: 'Tambah Data',
        image: '',
      };
      data = [plusData].concat(data);
    }
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
    while (
      numberOfElementsLastRow !== numColumns &&
      numberOfElementsLastRow !== 0
    ) {
      data.push({key: `blank-${numberOfElementsLastRow}`, empty: true});
      numberOfElementsLastRow++;
    }
    return data;
  };

  const onOpenInputTes = (item) => {
    if (item.id === '1') {
      navigation.navigate('InputTes');
    } else {
      navigation.navigate('DetailTes', {tes: item});
    }
  };
  return (
    <View>
      {/* <Text>List Menu</Text> */}
      <FlatList
        data={formatData(props.data, 2)}
        // style={{flex: 1}}
        numColumns={2}
        renderItem={({item, index}) => (
          <TouchableOpacity
            key={index}
            style={{
              flex: 1,
              // borderWidth: 1,
              alignItems: 'center',
              marginBottom: hp(0.5),
            }}
            onPress={() => {
              onOpenInputTes(item);
            }}>
            {item.empty ? null : item.id === '1' ? (
              <Card
                style={{
                  borderRadius: wp(2),
                  width: wp(42),
                  height: wp(38),
                  backgroundColor: colors.primary,
                }}>
                <LinearGradient
                  start={{x: 0.0, y: 0.25}}
                  end={{x: 0.5, y: 1.0}}
                  colors={[colors.primary, colors.bgErm]}
                  style={{
                    borderRadius: wp(2),
                    height: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      padding: wp(1),
                      height: wp(9.5),
                      width: wp(9.5),
                      borderRadius: wp(6),
                      backgroundColor: colors.primarydark,
                      alignItems: 'center',
                      justifyContent: 'center',
                      shadowColor: '#000',
                      shadowOffset: {
                        width: 0,
                        height: 2,
                      },
                      shadowOpacity: 0.25,
                      shadowRadius: 3.84,

                      elevation: 5,
                      marginBottom: hp(0.5),
                    }}>
                    <FontAweSome
                      name="plus"
                      color={colors.white}
                      size={hp(3)}
                    />
                  </View>
                  <Text
                    style={{
                      color: colors.primarydark,
                      fontSize: wp(3.2),
                      fontFamily: 'JosefinSans-Bold',
                    }}>
                    TAMBAH TES
                  </Text>
                </LinearGradient>
              </Card>
            ) : (
              <Card
                style={{
                  borderRadius: wp(2),
                }}>
                <Image
                  source={{uri: item.foto}}
                  style={{
                    borderRadius: wp(2),
                    width: wp(42),
                    height: wp(38),
                  }}
                  resizeMode={'cover'}
                />
                <View
                  style={{
                    position: 'absolute',
                    bottom: 0,
                    width: wp(42),
                    height: '26%',
                    borderBottomLeftRadius: wp(2),
                    borderBottomRightRadius: wp(2),
                    paddingBottom: wp(1),
                    paddingTop: wp(1.2),
                    backgroundColor: 'rgba(41, 88, 133, 0.9)',
                    paddingLeft: wp(1.5),
                    paddingEnd: wp(7),
                  }}>
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={2}
                    style={{
                      color: 'white',
                      fontSize: wp(3),
                      fontFamily: 'JosefinSans-MediumItalic',
                    }}>
                    {item.nama}
                  </Text>
                </View>
                <View
                  style={{
                    position: 'absolute',
                    bottom: 25,
                    right: 3,
                    padding: wp(1),
                    height: wp(9.5),
                    width: wp(9.5),
                    borderRadius: wp(6),
                    backgroundColor: colors.primary,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <FontAweSome name="plus" color={colors.white} size={hp(2)} />
                </View>
              </Card>
            )}
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default TestList;
