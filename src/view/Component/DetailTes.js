/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  AsyncStorage,
} from 'react-native';
import {Container, Card} from 'native-base';
import ReLoading from '../../component/ReLoading';

import colors from '../../res/colors';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';

import ReModal from 'react-native-modal';

import {useNavigation, useRoute} from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const initialState = {
  loading: false,
  visibleSideBar: false,
  id: '',
  foto: '',
  nama: '',
  deskripsi: '',

  role: '',
  user: null,
};

const DetailTes = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const [state, setState] = useState(initialState);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const preparedScreen = async () => {
    await AsyncStorage.getItem('userData').then((item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        updateState({
          user: user,
        });
      }
    });
  };

  const checkParamsData = async () => {
    await updateState({loading: true});
    preparedScreen();
    const tesDetail = (await route.params) ? route.params.tes : null;
    if (tesDetail !== null) {
      await updateState({
        id: tesDetail.id,
        foto: tesDetail.foto,
        nama: tesDetail.nama,
        deskripsi: tesDetail.deskripsi,
        createAt: tesDetail.createAt,
      });
    }
    await updateState({loading: false});
  };

  useEffect(() => {
    checkParamsData();
  }, []);

  const toggleVisibleInfo = () => {
    updateState({visibleInfo: !state.visibleInfo});
  };

  const ButtonLinier = ({label, onPress}) => {
    return (
      <TouchableOpacity onPress={onPress}>
        <Card
          style={{
            borderRadius: wp(1.5),
            width: wp(80),
            height: wp(11),
            backgroundColor: colors.primary,
          }}>
          <LinearGradient
            start={{x: 0.0, y: 0.25}}
            end={{x: 0.5, y: 1.0}}
            colors={[colors.primary, colors.bgErm]}
            style={{
              borderRadius: wp(1.5),
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: colors.primarydark,
                fontSize: wp(4.2),
                fontFamily: 'JosefinSans-Bold',
              }}>
              {label}
            </Text>
          </LinearGradient>
        </Card>
      </TouchableOpacity>
    );
  };

  return (
    <Container>
      <StatusBar translucent={false} />

      <ImageBackground
        style={{flex: 1, backgroundColor: 'white'}}
        source={require('../../res/image/background.jpg')}>
        {/* <DeafaultHeader title="Detail Tes" backButton /> */}
        {state.loading ? (
          <ReLoading />
        ) : (
          <View style={{padding: wp(-2)}}>
            <View>
              <View
                style={{
                  position: 'absolute',
                  top: 0,
                  width: '100%',
                  justifyContent: 'center',
                  zIndex: 1000,
                  flexDirection: 'row',
                  alignItems: 'center',
                  backgroundColor: 'rgba(0,0,0,0.6)',
                  paddingVertical: hp(1),
                }}>
                <View style={{position: 'absolute', zIndex: 1200, left: 15}}>
                  <TouchableOpacity
                    onPress={() => {
                      navigation.goBack();
                    }}>
                    <FontAweSome
                      name="arrow-left"
                      color={colors.white}
                      size={hp(3.2)}
                    />
                  </TouchableOpacity>
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                    textAlignVertical: 'center',
                    flex: 6,
                    fontSize: hp(3),
                    color: colors.white,
                    fontFamily: 'JosefinSans-Bold',
                  }}>
                  Detail Tes
                </Text>
              </View>
              <Image
                source={
                  state.foto
                    ? {uri: state.foto}
                    : require('../../res/image/bg.jpeg')
                }
                style={{
                  // borderRadius: wp(2),
                  // marginTop: hp(-10),
                  width: wp(100),
                  height: wp(65),
                }}
                resizeMode={'cover'}
              />
              <View style={{position: 'absolute', bottom: 10, width: wp(100)}}>
                <View style={{alignItems: 'center', marginHorizontal: wp(10)}}>
                  <Text
                    style={{
                      backgroundColor: 'rgba(0,0,0,0.5)',
                      paddingHorizontal: wp(2),
                      paddingVertical: hp(0.3),
                      color: colors.white,
                      fontSize: wp(4.2),
                      fontFamily: 'JosefinSans-Medium',
                      textAlign: 'center',
                    }}>
                    {state.nama}
                  </Text>
                </View>
              </View>
            </View>
            <View style={{alignItems: 'center', marginVertical: hp(4)}}>
              <View style={{width: wp(80), marginVertical: hp(2)}}>
                <ButtonLinier
                  label="Informasi Tes"
                  onPress={() => {
                    toggleVisibleInfo();
                  }}
                />
              </View>
              <View style={{width: wp(80), marginVertical: hp(2)}}>
                <ButtonLinier
                  label="Buat Penilaian Tes"
                  onPress={async () => {
                    navigation.navigate('InputNilaiTes', {
                      namaTes: {nama: state.nama, id: state.id},
                    });
                  }}
                />
              </View>
              {state.user?.role === 'Admin' ? (
                <View style={{width: wp(80), marginVertical: hp(2)}}>
                  <ButtonLinier
                    label="Laporan Hasil Tes"
                    onPress={() => {
                      navigation.navigate('LaporanTes');
                    }}
                  />
                </View>
              ) : null}
            </View>

            <View style={{height: hp(10)}} />
          </View>
        )}
        <ReModal
          isVisible={state.visibleInfo}
          onBackdropPress={() => {
            toggleVisibleInfo();
          }}
          onBackButtonPress={() => {
            toggleVisibleInfo();
          }}
          style={{
            alignItems: 'center',
            justifyContent: 'flex-start',
            paddingVertical: hp(2),
          }}
          backdropOpacity={0.7}
          animationIn={'fadeIn'}
          animationOut={'fadeOut'}
          useNativeDriver={true}>
          <View
            style={{
              backgroundColor: 'white',
              borderRadius: wp(1),
              padding: hp('1%'),
              width: wp(90),
              minHeight: hp(30),
              maxHeight: hp(87),
            }}>
            <View
              style={{
                paddingTop: hp(1),
                paddingBottom: hp(1.5),
                borderBottomWidth: 0.5,
                borderColor: 'gray',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: wp(4), fontWeight: 'bold'}}>
                INFORMASI TES
              </Text>
            </View>
            <ScrollView
              style={{paddingVertical: hp(1), paddingHorizontal: wp(1.5)}}>
              <View style={{marginBottom: hp(0.5)}}>
                <Text
                  style={{
                    color: colors.primarydark,
                    fontSize: wp(3.5),
                    fontFamily: 'Poppins-Regular',
                  }}>
                  {state.nama}
                </Text>
              </View>
              <View style={{}}>
                <Text
                  style={{
                    color: colors.gray08,
                    fontSize: wp(3.5),
                    fontFamily: 'Poppins-Regular',
                  }}>
                  {state.deskripsi}
                </Text>
              </View>
            </ScrollView>
          </View>
        </ReModal>
      </ImageBackground>
    </Container>
  );
};

export default DetailTes;
