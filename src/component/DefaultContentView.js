import {View, Text} from 'react-native';
import React from 'react';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const DefaultContentView = (props) => {
  return (
    <View
      style={{
        paddingHorizontal: widthPercentageToDP(4),
        paddingTop: heightPercentageToDP(2),
        ...props.style,
      }}>
      {props.children}
    </View>
  );
};

export default DefaultContentView;
