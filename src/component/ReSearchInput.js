/* eslint-disable react-native/no-inline-styles */
import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {TextInput} from 'react-native-gesture-handler';
import colors from '../res/colors';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ReSearchInput = ({style, ...props}) => {
  return (
    <View
      style={{
        borderWidth: 1,
        borderRadius: wp(1),
        flexDirection: 'row',
        alignItems: 'center',
        ...style,
      }}>
      <TextInput
        value={props.value}
        placeholderTextColor={colors.gray11}
        style={{
          flex: 1,
          paddingHorizontal: wp(2),
          // height: hp(5),
          paddingVertical: hp(0.5),
          fontSize: wp(3.5),
          fontFamily: 'Poppins-Regular',
          color: colors.textGray,
        }}
        placeholder={props.placeholder || 'Search'}
        onChangeText={props.onChangeText}
      />
      <FontAwesome
        name="search"
        color={colors.gray10}
        size={wp(4.2)}
        style={{
          marginRight: wp(2),
          textAlignVertical: 'center',
        }}
      />
    </View>
  );
};

export default ReSearchInput;
