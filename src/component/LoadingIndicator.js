import React from 'react';

import {View, ActivityIndicator, Text} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import colors from '../res/colors';

const LoadingIndicator = () => {
  return (
    <View style={{marginVertical: heightPercentageToDP(3)}}>
      <ActivityIndicator size={'large'} color={colors.secondary} />
    </View>
  );
};

export default LoadingIndicator;
