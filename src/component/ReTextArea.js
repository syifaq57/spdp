/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Animated,
  StyleSheet,
} from 'react-native';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import colors from '../res/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Textarea} from 'native-base';

const ReTextArea = (props) => {
  const inputref = React.useRef(null);

  const initialState = {
    isFieldActive: false,
    position: new Animated.Value(props.value ? 1 : 0),
    secureText: true,
    iconPass: 'eye-slash',
  };
  const [state, setState] = React.useState(initialState);

  const [labelStyle, setLabelStyle] = React.useState({
    position: 'absolute',
    fontSize: wp(3.5),
    color: colors.gray11,
    fontFamily: 'Poppins-Regular',
  });

  const updateState = (newData) => {
    setState((prev) => ({
      ...prev,
      ...newData,
    }));
  };
  const titleAnimated = () => {
    return {
      top: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [hp(-1.5), hp(-1.5)],
      }),
      fontSize: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [wp(0), wp(3)],
      }),
      marginLeft: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [wp(0), wp(0)],
      }),
      color: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [colors.gray08, colors.gray08],
      }),
    };
  };
  const handleFocus = () => {
    if (!state.isFieldActive) {
      setState({...state, isFieldActive: true});
      Animated.timing(state.position, {
        toValue: 1,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  const handleBlur = () => {
    if (state.isFieldActive && !props.value) {
      setState({...state, isFieldActive: false});
      Animated.timing(state.position, {
        toValue: props.value ? 1 : 0,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  return (
    <View pointerEvents={props.disabled ? 'none' : 'auto'}>
      <View
        style={[
          {
            minHeight: hp(7.8),
            borderWidth: 2,
            borderColor: colors.gray02,
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: wp(2),
          },
        ]}>
        <View style={{flex: 1}}>
          <Textarea
            ref={inputref}
            value={props.value}
            placeholder={props.placeholder}
            placeholderTextColor={props.labelColor}
            multiline={props.multiLine}
            rowSpan={5}
            keyboardType={props.numeric ? 'numeric' : 'default'}
            editable={props.editable}
            secureTextEntry={props.passwordInput ? state.secureText : false}
            onFocus={() => {
              handleFocus();
            }}
            onChangeText={(text) => props.onChangeText(text)}
            onBlur={() => {
              handleBlur();
            }}
            style={{
              fontSize: wp(4),
              paddingTop: hp(1),
              paddingBottom: hp(-0.6),
              fontFamily: 'Poppins-Regular',
              color: props.valueColor ? props.valueColor : colors.gray08,
            }}
          />
        </View>
      </View>

      {props.noAnimation ? (
        <Text
          style={{
            marginLeft: wp(0.2),
            position: 'absolute',
            top: hp(-1.5),
            fontSize: wp(2.8),
            color: props.labelColor ? props.labelColor : colors.primarydark,
            fontFamily: 'Poppins-Regular',
          }}>
          {props.label}
        </Text>
      ) : (
        <Animated.Text
          onPress={() => {
            inputref.current.focus();
          }}
          style={[labelStyle, titleAnimated()]}>
          {' '}
          {props.label}{' '}
        </Animated.Text>
      )}
    </View>
  );
};

export default ReTextArea;
