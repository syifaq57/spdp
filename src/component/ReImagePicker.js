/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import {InputGroup} from 'native-base';
import React, {
  useEffect,
  useState,
  useImperativeHandle,
  forwardRef,
} from 'react';

import {View, Image, TouchableOpacity, Text} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ReModalImagePicker from './ReModalImagePicker';

const initialState = {
  image: '',
  imageObject: null,

  visiblePicker: false,
};

const ReImagePicker = ({...props}, ref) => {
  const [state, setState] = useState(initialState);

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const togglePicker = () => {
    updateState({visiblePicker: !state.visiblePicker});
  };

  const getImage = () => {
    return {
      image: state.image,
      imageObject: state.imageObject,
    };
  };

  useEffect(() => {
    if (props.defaultImage) {
      updateState({
        image: props.defaultImage,
      });
    }
  }, []);

  useImperativeHandle(ref, () => ({
    getImage,
  }));

  return (
    <View style={{marginVertical: hp(3), ...props.style}}>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
            togglePicker();
          }}>
          {state.image.length > 0 ? (
            <Image
              source={{
                uri: state.image,
              }}
              style={{
                height: hp(12),
                width: hp(12),
                borderRadius: hp(22),
                resizeMode: 'cover',
              }}
            />
          ) : (
            <Image
              source={require('../res/images/Profile.png')}
              style={{
                height: hp(12),
                width: hp(12),
                borderRadius: hp(15),
                resizeMode: 'cover',
              }}
            />
          )}
        </TouchableOpacity>
        <Text
          style={{
            fontSize: wp(4),
            // fontWeight: 'bold',
            marginTop: hp(1.5),
          }}>
          Edit Foto
        </Text>
      </View>
      <ReModalImagePicker
        visible={state.visiblePicker}
        onCancel={() => togglePicker()}
        onPickImage={(image) => {
          togglePicker();
          updateState({
            imageObject: image,
            image: image.uri,
          });
        }}
      />
    </View>
  );
};

export default forwardRef(ReImagePicker);
