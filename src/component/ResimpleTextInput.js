// /* eslint-disable react-native/no-inline-styles */
// import React from 'react';
// import {
//   View,
//   Text,
//   TouchableOpacity,
//   TextInput,
//   Animated,
//   StyleSheet,
// } from 'react-native';
// import FontAweSome from 'react-native-vector-icons/FontAwesome';
// import colors from '../res/colors/index';
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp,
// } from 'react-native-responsive-screen';
// import NumberToRupiah from '../view/Calculator/NumberToRupiah';

// const ReSimpleTextInput = (props) => {
//   const getValue = () => {
//     let result = props.value;
//     if (props.value && props.value != null && props.currency) {
//       return NumberToRupiah(props.value);
//     }

//     if (typeof props.value !== 'string' && props.value != null) {
//       return String(props.value);
//     }

//     return result;
//   };

//   const onChangeText = (text) => {
//     let result = text;
//     if (props.numeric) {
//       result = result.toString().replace(/[a-z]/g, '').replace(',', '.');
//     }

//     props.onChangeText(result);
//   };

//   return (
//     <View style={{...props.style}}>
//       {!props.hideLabel && (
//         <Text
//           style={{
//             fontSize: wp(2.9),
//             color: colors.textPrimary,
//             marginLeft: wp(0.9),
//             marginBottom: hp(0.3),
//           }}>
//           {props.label}
//         </Text>
//       )}

//       <View
//         style={{
//           borderWidth: 1.5,
//           borderRadius: wp(0.8),
//           justifyContent: 'center',
//           borderColor: colors.secondary,
//           backgroundColor: !props.disabled
//             ? colors.secondary
//             : colors.textSecond,
//           paddingHorizontal: wp(1),
//           paddingRight: wp(2),
//           flexDirection: 'row',
//         }}>
//         {props.currency && (
//           <Text
//             style={{
//               fontSize: wp(3.5),
//               fontWeight: 'bold',
//               color: colors.bluePrimary,
//               textAlignVertical: 'center',
//               marginRight: wp(0.1),
//             }}>
//             Rp-,
//           </Text>
//         )}
//         <TextInput
//           value={getValue()}
//           multiline={props.multiLine || props.textArea}
//           keyboardType={props.numeric || props.currency ? 'numeric' : 'default'}
//           editable={!props.disabled}
//           onChangeText={(text) => onChangeText(text)}
//           placeholderTextColor={colors.textSecond}
//           style={{
//             flex: 1,
//             fontSize: wp(3.5),
//             fontFamily: 'Poppins-Regular',
//             color: colors.textPrimary,
//             height: props.textArea
//               ? hp(20)
//               : props.height
//               ? props.height
//               : hp(5),
//             textAlignVertical: props.textArea ? 'top' : 'center',
//           }}
//           placeholder={
//             props.placeholder
//               ? props.placeholder
//               : `Input ${props.label.toLowerCase()}`
//           }
//           secureTextEntry={props.passwordInput ? true : false}
//           onBlur={() => {
//             if (props.onBlur) {
//               props.onBlur();
//             }
//           }}
//         />
//         {props.inputPersen && (
//           <FontAweSome
//             name="percent"
//             size={hp(2)}
//             style={{color: colors.bluePrimary, textAlignVertical: 'center'}}
//           />
//         )}
//         {props.searchIcon && (
//           <FontAweSome
//             name="search"
//             size={hp(2)}
//             style={{color: colors.bluePrimary, textAlignVertical: 'center'}}
//           />
//         )}
//         {props.rightContent ? props.rightContent : null}
//       </View>
//       {props.erorrMessage || props.warningMessage ? (
//         <Text
//           numberOfLines={2}
//           ellipsizeMode="tail"
//           style={{
//             marginLeft: wp(1),
//             fontSize: wp(3),
//             color: props.warningMessage ? colors.Orange : colors.danger,
//           }}>
//           {props.warningMessage || props.erorrMessage}
//         </Text>
//       ) : null}
//     </View>
//   );
// };

// export default ReSimpleTextInput;
