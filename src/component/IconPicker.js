import {View, Text} from 'react-native';
import React from 'react';
import {Picker} from 'native-base';

import colors from '../res/colors/index';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import FontAweSome from 'react-native-vector-icons/FontAwesome';

const IconPicker = ({onValueChange, style, items}) => {
  const getItems = () => {
    return items && items.length > 0 ? items : [];
  };
  return (
    <View
      style={{
        backgroundColor: colors.secondary,
        borderRadius: wp(1.5),
        width: wp(11),
        height: hp(5.5),
        ...style,
      }}>
      <Picker
        enabled={true}
        selectedValue={'aaa'}
        mode="dropdown"
        style={{
          opacity: 0,
        }}
        onValueChange={(itemValue, itemIndex) => {
          onValueChange(itemValue);
        }}>
        <Picker.Item
          style={{borderBottomWidth: 1}}
          label={'Search By'}
          value={''}
        />

        {getItems().map((item, index) => (
          <Picker.Item key={index} label={item.label} value={item.value} />
        ))}
      </Picker>
      <FontAweSome
        style={{position: 'absolute', left: wp(3.2), top: hp(1.4)}}
        name="chevron-down"
        color={colors.white}
        size={hp(2.5)}
      />
    </View>
  );
};

export default IconPicker;
