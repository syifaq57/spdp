/* eslint-disable react-native/no-inline-styles */
import {View, Text} from 'react-native';
import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

const ReCardView = (props) => {
  return (
    <View
      style={{
        padding: wp(2),
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 4,
        },
        shadowOpacity: 0.3,
        shadowRadius: 4.65,

        elevation: 8,
        ...props.style,
      }}>
      {props.children}
    </View>
  );
};

export default ReCardView;
