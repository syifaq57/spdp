import {View, Text, StatusBar} from 'react-native';
import React from 'react';
import {Container} from 'native-base';

import colors from '../res/colors';

const DefaultContainer = (props) => {
  return (
    <View style={{flex: 1}}>
      <View>
        <StatusBar translucent={false} backgroundColor={colors.secondary} />
      </View>
      {props.children}
    </View>
  );
};

export default DefaultContainer;
