/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {View, Image, AsyncStorage, Alert, TouchableOpacity} from 'react-native';
import {Text} from 'native-base';
import colors from '../res/colors/index';
import Modal from 'react-native-modal';
// import Rate, {AndroidMarket} from 'react-native-rate';
import FontAweSome from 'react-native-vector-icons/FontAwesome';

import {useNavigation, useFocusEffect} from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const listMenu = [
  {
    id: 'dashboard',
    title: 'Dashboard',
    icon: 'home',
  },
  {
    id: 'datauser',
    title: 'Data Master',
    icon: 'user',
  },
  {
    id: 'tes',
    title: 'Informasi & Data Tes',
    icon: 'archive',
  },
  {
    id: 'laporan',
    title: 'Laporan & Hasil Tes',
    icon: 'bar-chart',
  },
  {
    id: 'logout',
    title: 'Logout',
    icon: 'power-off',
  },
];

const listMenuUser = [
  {
    id: 'dashboard',
    title: 'Dashboard',
    icon: 'home',
  },
  {
    id: 'logout',
    title: 'Logout',
    icon: 'power-off',
  },
];

const SideBar = (props) => {
  const navigation = useNavigation();
  const [state, setState] = useState({
    user: null,
    listMenu: listMenu,
  });

  const updateState = (newData) => {
    setState((prev) => ({...prev, ...newData}));
  };

  const onLogout = async () => {
    await AsyncStorage.clear();
    navigation.reset({
      index: 0,
      routes: [{name: 'LoginScreen'}],
    });
    navigation.navigate('LoginScreen');
  };

  const logoutAlert = () => {
    Alert.alert('Logout!', 'Apakah Kamu yakin ingin Logout ?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {text: 'YES', onPress: () => onLogout()},
    ]);
  };

  const onSelectMenu = (menu) => {
    switch (menu) {
      case 'dashboard':
        navigation.navigate('HomeAdmin');
        break;
      case 'datauser':
        navigation.navigate('DataUser');
        break;
      case 'tes':
        navigation.navigate('DataTes');
        break;
      case 'laporan':
        navigation.navigate('LaporanTes');
        break;
      case 'logout':
        logoutAlert();
        break;
      default:
        break;
    }
  };

  const preparedScreen = async () => {
    await AsyncStorage.getItem('userData').then((item) => {
      const user = JSON.parse(item);
      if (user && Object.keys(user).length > 0) {
        updateState({
          user: user,
        });
      }
    });
  };

  const getListMenu = () => {
    if (state.user?.role === 'User') {
      return listMenuUser;
    }
    return listMenu;
  };

  useEffect(() => {
    preparedScreen();
  }, []);

  return (
    <View>
      <Modal
        isVisible={props.isVisible}
        style={{marginVertical: 0, marginHorizontal: 0}}
        backdropOpacity={0.3}
        animationIn={'slideInLeft'}
        animationOut={'slideOutLeft'}
        onBackdropPress={props.onBackButtonPress}
        onBackButtonPress={props.onBackButtonPress}>
        <View
          style={{
            backgroundColor: 'white',
            height: '100%',
            width: '65%',
            // alignItems: 'center',
            // paddingTop: hp(10),
          }}>
          <View
            style={{
              flex: 2,
              alignItems: 'center',
              backgroundColor: colors.primarydark,
              justifyContent: 'center',
            }}>
            <Image
              source={
                state.user?.foto
                  ? {uri: state.user?.foto}
                  : require('../res/image/Profile.png')
              }
              style={{
                width: wp(22),
                height: wp(22),
                borderRadius: wp(25),
                resizeMode: 'cover',
                marginBottom: hp(1),
              }}
            />
            <Text
              style={{
                fontSize: wp(4.3),
                textAlign: 'center',
                color: 'white',
                fontFamily: 'JosefinSans-Bold',
              }}>
              {state.user?.nama}
            </Text>
          </View>
          <View style={{flex: 6, paddingTop: hp(2), marginHorizontal: wp(4)}}>
            {getListMenu().map((data, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  props.onBackButtonPress();
                  onSelectMenu(data.id);
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    marginVertical: hp(2),
                  }}>
                  <View
                    style={{
                      flex: 0.5,
                      alignItems: 'center',
                      marginRight: wp(2.5),
                      marginTop: hp(0.2),
                    }}>
                    <FontAweSome
                      name={data.icon}
                      color={colors.black}
                      size={hp(3)}
                    />
                  </View>

                  <Text
                    style={{
                      flex: 5,
                      fontSize: wp(4),
                      fontWeight: 'bold',
                      color: colors.grayPrimary,
                    }}>
                    {data.title}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default SideBar;
