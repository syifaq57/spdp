/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Animated,
  StyleSheet,
} from 'react-native';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import colors from '../res/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ReTextInput = (props) => {
  const defaultFonSize = wp(3.5);

  const defaultLableColor = props.loginScreen
    ? [
        props.labelColor ? props.labelColor : colors.textSecondary,
        colors.textPrimary,
      ]
    : [
        props.labelColor ? props.labelColor : colors.textTertiary,
        colors.secondary,
      ];

  const inputref = React.useRef(null);

  const initialState = {
    isFieldActive: false,
    position: new Animated.Value(0),
    secureText: true,
    iconPass: 'eye-slash',
  };
  const [state, setState] = React.useState(initialState);

  const [labelStyle, setLabelStyle] = React.useState({
    position: 'absolute',
    fontSize: defaultFonSize,
    color: colors.gray10,
    fontFamily: 'Poppins-Regular',
  });

  const updateState = (newData) => {
    setState((prev) => ({
      ...prev,
      ...newData,
    }));
  };
  const titleAnimated = (labelColor) => {
    return {
      top: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [hp(3), hp(0.5)],
      }),
      fontSize: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [defaultFonSize, wp(3)],
      }),
      marginLeft: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [wp(0.1), wp(-0.5)],
      }),
      color: state.position.interpolate({
        inputRange: [0, 1],
        outputRange: defaultLableColor,
      }),
    };
  };
  const handleFocus = () => {
    if (!state.isFieldActive) {
      setState({...state, isFieldActive: true});
      Animated.timing(state.position, {
        toValue: 1,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  const handleBlur = () => {
    if (state.isFieldActive && !props.value) {
      setState({...state, isFieldActive: false});
      Animated.timing(state.position, {
        toValue: props.value ? 1 : 0,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  const handleShowPassword = () => {
    if (state.iconPass === 'eye') {
      updateState({iconPass: 'eye-slash', secureText: true});
    } else {
      updateState({iconPass: 'eye', secureText: false});
    }
  };

  useEffect(() => {
    if (props.value) {
      updateState({
        position: new Animated.Value(1),
      });
    }
  }, [props.value]);

  return (
    <View pointerEvents={props.disabled ? 'none' : 'auto'}>
      <View
        style={{
          width: '100%',
          minHeight: hp(7.4),
          borderBottomWidth: props.loginScreen ? 3 : 2,
          borderColor: props.loginScreen ? colors.white : colors.textTertiary,
          flexDirection: 'row',
          alignItems: 'center',
          // borderWidth: 1,
          ...props.style,
        }}>
        <View style={{flex: 1}}>
          <TextInput
            ref={inputref}
            value={props.value}
            // placeholder={props.placeholder}
            multiline={props.multiLine}
            // maxLength={props.numeric ? 3 : 1000}
            keyboardType={props.numeric ? 'numeric' : 'default'}
            editable={props.editable}
            secureTextEntry={props.passwordInput ? state.secureText : false}
            onFocus={() => {
              handleFocus();
            }}
            onChangeText={(text) => props.onChangeText(text)}
            onBlur={() => {
              handleBlur();
            }}
            placeholderTextColor={colors.gray11}
            style={{
              fontSize: defaultFonSize,
              paddingTop: hp(2.5),
              paddingBottom: hp(-0.6),
              fontFamily: 'Poppins-Regular',
              color: props.valueColor ? props.valueColor : colors.textGray,
            }}
            placeholder={props.placeholder}
          />
        </View>
        {props.passwordInput ? (
          <View style={{marginTop: hp(2)}}>
            <TouchableOpacity onPress={() => handleShowPassword()}>
              <FontAweSome
                name={state.iconPass}
                color={
                  props.loginScreen ? colors.textSecondary : colors.textTertiary
                }
                size={wp(5)}
              />
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
      {props.placeholder ? (
        <Text
          style={{
            position: 'absolute',
            top: hp(0.1),
            fontSize: wp(2.8),
            marginLeft: wp(0.1),
            color: props.labelColor ? props.labelColor : colors.gray08,
            fontFamily: 'Poppins-Regular',
          }}>
          {props.label}
        </Text>
      ) : (
        <Animated.Text
          onPress={() => {
            inputref.current.focus();
          }}
          style={[labelStyle, titleAnimated(props.labelColor)]}>
          {' '}
          {props.label}{' '}
        </Animated.Text>
      )}
    </View>
  );
};

export default ReTextInput;
