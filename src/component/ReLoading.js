/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, Image} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../res/colors';

const ReLoading = () => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: hp(-10),
      }}>
      <Image
        style={{
          width: hp('10%'),
          height: hp('10%'),
          resizeMode: 'contain',
        }}
        source={require('../res/image/running.gif')}
      />
      <Text
        style={{
          fontSize: hp(2.2),
          color: colors.gray10,
          fontFamily: 'Poppins-Regular',
        }}>
        Loading...
      </Text>
    </View>
  );
};

export default ReLoading;
