/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Text, Header} from 'native-base';
import colors from '../res/colors/index';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const DeafaultHeader = (props) => {
  const navigation = useNavigation();
  return (
    <Header
      style={{
        backgroundColor: colors.secondary,
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          flexDirection: 'row',
          marginHorizontal: wp(3),
          justifyContent: 'center',
        }}>
        {props.backButton ? (
          <View style={{marginRight: wp(4)}}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <FontAweSome
                name="arrow-left"
                color={colors.textSecondary}
                size={hp(3)}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        <Text
          style={{
            // textAlign: 'center',
            textAlignVertical: 'center',
            flex: 6,
            fontSize: hp(3),
            color: colors.textSecondary,
            fontFamily: 'JosefinSans-Bold',
          }}>
          {props.title}
        </Text>
        {props.rightButton ? (
          <TouchableOpacity
            style={{marginTop: hp(0.5)}}
            onPress={props.onRightButtonPress}>
            <FontAweSome
              name={props.rightButton.length > 0 ? props.rightButton : 'gear'}
              color={colors.textSecondary}
              size={props.righButtonSize ? props.righButtonSize : hp(3)}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    </Header>
  );
};

export default DeafaultHeader;
