import {View, Text} from 'react-native';
import React from 'react';

import uuid from 'react-native-uuid';
import storage from '@react-native-firebase/storage';

const ImageUploader = async (foto) => {
  try {
    if (!foto) {
      return '';
    } else {
      const uri = foto;
      const ext = uri.split('.').pop();

      const filename = `${uuid.v1()}.${ext}`;

      const imageRef = storage().ref(`foto/${filename}`);

      await imageRef.putFile(uri);

      const url = await imageRef.getDownloadURL();

      return url;
    }
  } catch (e) {
    console.log('error upload', e);
  }
};

export default ImageUploader;
