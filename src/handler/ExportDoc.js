import {Alert, PermissionsAndroid} from 'react-native';

import RNFS, {writeFile} from 'react-native-fs';

import FileViewer from 'react-native-file-viewer';

import XLSX from 'xlsx';

import uuid from 'react-native-uuid';

const exportExcel = async (data, date) => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: 'Akses Dokumen',
        message: 'Aplikasi membutuhkan akses untuk menyimpan file',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      const ws = await XLSX.utils.json_to_sheet(data);
      const wb = await XLSX.utils.book_new();

      await XLSX.utils.book_append_sheet(wb, ws, 'SheetJS');

      ws['!cols'] = [
        {width: 10},
        {width: 20},
        {width: 25},
        {width: 20},
        {width: 20},
        {width: 20},
        {width: 30},
      ];
      // ws['!rows'] = [{}];

      const wbout = await XLSX.write(wb, {type: 'binary', bookType: 'xlsx'});
      const file =
        RNFS.DownloadDirectoryPath + '/' + `Pemilih_${date}_${uuid.v1()}.xlsx`;
      await writeFile(file, wbout, 'ascii')
        .then(async () => {
          Alert.alert('Export Excel', `File telah di export ke ${file}?`, [
            {
              text: 'Tutup',
              onPress: () => {
                // return true;
              },
              style: 'cancel',
            },
            {
              text: 'Buka File',
              onPress: () => {
                FileViewer.open(file);
              },
            },
          ]);
        })
        .catch((err) => {
          Alert.alert('exportFile Error', 'Error ' + err.message);
        });
    } else {
      console.log('permission denied');
    }
  } catch (err) {
    console.warn(err);
  }
  /* convert AOA back to worksheet */
};

export default exportExcel;
