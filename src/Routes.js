import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import LoginScreen from './view/Login/LoginScreen';

import SplashScreen from './view/SplashScreen/SplashScreen';
import HomeScreen from './view/Home/HomeScreen';
import ProfileScreen from './view/Profile/ProfileScreen';
import UserScreen from './view/User/UserScreen';
import TambahUser from './view/User/TambahUser';
import ExportImportScreen from './view/ExporImport/ExportImportScreen';
import PemilihScreen from './view/Pemilih/PemilihScreen';
import FormPemilih from './view/Pemilih/FormPemilih';
import ValidasiScreen from './view/Validasi/ValidasiScreen';
import TambahPemilih from './view/Pemilih/TambahPemilih';

const Stack = createStackNavigator();

const Route = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="SplashScreen"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
        <Stack.Screen name="UserScreen" component={UserScreen} />
        <Stack.Screen name="TambahUser" component={TambahUser} />
        <Stack.Screen
          name="ExportImportScreen"
          component={ExportImportScreen}
        />
        <Stack.Screen name="PemilihScreen" component={PemilihScreen} />
        <Stack.Screen name="FormPemilih" component={FormPemilih} />
        <Stack.Screen name="TambahPemilih" component={TambahPemilih} />
        <Stack.Screen name="ValidasiScreen" component={ValidasiScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;
